//Project Includes
#include <arf_encoder/encoder.hpp>
#include <nav_msgs/Odometry.h>
#include "tf/transform_listener.h"
#include <geometry_msgs/Twist.h>

//System Includes
#include <iostream>
#include <bitset>



using namespace CAS_IRTWalkerFramework;

//Constructor
PrydeEncoder::PrydeEncoder() : m_exitFlag(false)
{
    std::cout << "PrydeEncoder constructor" << std::endl;
}

//Destructor
PrydeEncoder::~PrydeEncoder()
{
    std::cout << "PrydeEncoder destructor" << std::endl;
}

//Nodelet OnInit callback
void PrydeEncoder::onInit()
{
    std::cout << "PrydeEncoder::onInit()" << std::endl;

    m_initThread = boost::thread ( boost::bind ( &PrydeEncoder::OnInitImpl, this ) );

}
 
void PrydeEncoder::OnInitImpl ( void )
{

    m_nh = getNodeHandle();
    ros::NodeHandle local_nh = getPrivateNodeHandle();

    
    m_monitorThread = boost::thread ( boost::bind ( &PrydeEncoder::publishEncoderReadings, this ) );
    ros::spin();

	m_exitFlag = true;
}

void PrydeEncoder::publishEncoderReadings ( void )
{
    bool flag;

    {
        boost::mutex::scoped_lock l ( m_monitorMutex );
        flag = m_exitFlag;
    }
    while(!flag)
    {
        ros::spinOnce();
        sleep ( 1 );
        {
            boost::mutex::scoped_lock l ( m_monitorMutex );
            flag = m_exitFlag;
        }

    }

	std::cout << "Exiting PrydeEncoder::publishEncoderReadings" << std::endl;
}

// Register the nodelet
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS ( CAS_IRTWalkerFramework, PrydeEncoder, CAS_IRTWalkerFramework::PrydeEncoder, nodelet::Nodelet )

