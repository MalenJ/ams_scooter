#include <arf_encoder/encoder_node.hpp>


using namespace std;
using namespace CAS_IRTWalkerFramework;


PrydeEncoder::PrydeEncoder() : m_exitFlag(false), deviceNumber(0), deviceCount(0), m_usbresult(USB4_SUCCESS), m_sampleid(0)
{	

    std::cout << "PrydeEncoder constructor" << std::endl;
    std::cout << "ULONG_MAX= " << ULONG_MAX << std::endl;

    m_prevLeftEncoderCount=0;
    m_prevRightEncoderCount=0;


    m_xloc=0.0;
    m_yloc=0.0;
    m_theta=0.0;

#if 0
    //Initializing the USB interface 
    m_usbresult = USB4_Initialize(&deviceCount);

    // Check result code...
    if (m_usbresult != USB4_SUCCESS)
    {
        std::cout << "Failed to initialize USB4 driver!  Result code = " << m_usbresult << std::endl << std::flush;
        exit(-1);
    }
    else
    {
        if (deviceCount < 1)
        {
            std::cout << "No USB4 boards detected!" << std::endl << std::flush;
       	    exit(-1);
        }
        else
        {
            std::cout << "deviceCount=  " << deviceCount << std::endl << std::flush;
        }
    }

    //Open the log file
    //logfile.open ("encoderlog.txt"); 
#endif

}
 
//Destructor
PrydeEncoder::~PrydeEncoder()
{
    {
        boost::mutex::scoped_lock l ( m_monitorMutex );
        m_exitFlag = true;
    }

    //Shutting down the main thread
    m_monitorThread.interrupt();
    m_monitorThread.join();

#if 0
    // Step 10: Shutdown
    USB4_Shutdown();

	logfile.close(); 
#endif

    std::cout << "PrydeEncoder destructor" << std::endl;
}

void PrydeEncoder::start(void)
{

    m_encpub = m_nh.advertise<arf_msgs::encoder>("encoder_ticks",1);
    m_odompub = m_nh.advertise<nav_msgs::Odometry>("odom",1);


    //m_monitorThread = boost::thread ( boost::bind ( &PrydeEncoder::publishEncoderReadings, this ) );


    m_encsub = m_nh.subscribe("/encoder_ticks", 10, &PrydeEncoder::encoderCallback,this);

    
    ros::spin();
    std::cout << "Exiting ros::spin" << std::endl;
    m_exitFlag=false;
}

void PrydeEncoder::encoderCallback(const arf_msgs::encoder& encmsg) 
{
    std::cout << "encoder callback: x= "<< encmsg.ticks.x << ", y= " << encmsg.ticks.y << std::endl << std::flush;
    ros::Time now = ros::Time::now();   
    ros::Time sampletime = encmsg.header.stamp;

    static bool start=0;
    static float aest=0.0;

    std::cout << "leftEncoder= "<< encmsg.ticks.x << ", rightEncoder= " << encmsg.ticks.y << ", time= " << sampletime.toSec() << std::endl << std::flush;
    if(m_sampleid==0)
    {
        m_prevLeftEncoderCount=encmsg.ticks.x;
        m_prevRightEncoderCount=encmsg.ticks.y;
        m_lastsampletime = sampletime;
        m_sampleid = 1;
    }
    else
    {
        double update_x=0.0, update_y=0.0, update_th=0.0;

        m_sampleid  =  m_sampleid + 1;
        double del_lefttc = (double)encmsg.ticks.x - (double)m_prevLeftEncoderCount;
        double del_righttc = (-1.0)*((double)encmsg.ticks.y - (double)m_prevRightEncoderCount);


        if ((fabs(del_lefttc) > 20.0) || (fabs(del_righttc)>20.0))
        {
            double delta_t = (sampletime-m_lastsampletime).toSec();

            if (delta_t < 2.0)
            {
                std::cout << "del_lefttc= "<< del_lefttc << ", del_righttc= " << del_righttc << ", time_diff= " <<  delta_t << std::endl << std::flush;

                //calculate the distance the wheels have traveled, compared to last measurement.
                double del_leftdis = (del_lefttc*LDISTPERCOUNT)/1000.0; //in metres
                double del_rightdis= (del_righttc*RDISTPERCOUNT)/1000.0;//in metres
           
                std::cout << "del_leftdis= " << del_leftdis << ", del_rightdis= " << del_rightdis << std::endl << std::flush;
#if 0
                if(start==0)
                {
                    ldist=del_leftdis;
                    rdist=del_rightdis;
                    start=1;
                }
                else
                {
                    ldist=0.7*ldist+0.3*del_leftdis;
                    rdist=0.7*rdist+0.3*del_rightdis;
                }
                ldist=0.85*ldist+0.15*del_leftdis;
                rdist=0.85*rdist+0.15*del_rightdis;

                del_leftdis=ldist;
                del_rightdis=rdist;

#endif
                std::cout << "est_del_leftdis= " << del_leftdis << ", est_del_rightdis= "<< del_rightdis << std::endl << std::flush;
 

                //save the encoder readings for the next round
                m_prevLeftEncoderCount=encmsg.ticks.x;
                m_prevRightEncoderCount=encmsg.ticks.y;
                m_lastsampletime=sampletime;

        
                //Now compute the incremental velocity components
                //double delta_veltime_k=(( del_rightdis + del_leftdis )/2.0*delta_t);
                double delta_veltime_k=(( del_rightdis + del_leftdis )/2.0);
                double delta_wtime_k=(( del_rightdis - del_leftdis )/(WHEELBASE));

                aest=0.7*aest+0.3* delta_wtime_k;
                delta_wtime_k=aest;          
                std::cout << "delta_veltime_k= " << delta_veltime_k << ", delta_wtime_k= " << delta_wtime_k << std::endl << std::flush;

                update_x = delta_veltime_k*cos(m_theta+(delta_wtime_k/2.0));   
                update_y = delta_veltime_k*sin(m_theta+(delta_wtime_k/2.0));
                update_th=delta_wtime_k;

                //update_x = delta_veltime_k*delta_t*cos(m_theta);
                //update_y = delta_veltime_k*delta_t*sin(m_theta);
                //update_th=delta_wtime_k*delta_t;
                 
                std::cout << "update_x= " << update_x << ", update_y= " << update_y << ", update_th= " << update_th << std::endl << std::flush;
            }
            else
            {
                //save the encoder readings for the next round
                m_prevLeftEncoderCount=encmsg.ticks.x;
                m_prevRightEncoderCount=encmsg.ticks.y;
                m_lastsampletime=sampletime;
            }
        }
            
        std::cout << "x_k= "<< m_xloc << ", y_k= " << m_yloc << ", th_k= " << m_theta << std::endl << std::flush;

        //Estimate incremental x,y and theta changes - 2nd order Runge-Kutta
        m_xloc = m_xloc + update_x;
        m_yloc = m_yloc + update_y;
        m_theta = m_theta +  update_th;

        std::cout << "x_k+1= "<< m_xloc << ", y_k+1= " << m_yloc << ", th_k+1= " << m_theta << std::endl << std::endl << std::flush;
        //Publish an odometry message
        geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(m_theta);                         
     
        geometry_msgs::TransformStamped odom_trans;
        odom_trans.header.stamp = now;
        odom_trans.header.frame_id = "odom";
        odom_trans.child_frame_id = "base_link";

        odom_trans.transform.translation.x = m_xloc;
        odom_trans.transform.translation.y = m_yloc;
        odom_trans.transform.translation.z = 0.0;
        odom_trans.transform.rotation = odom_quat;

        //send the transform
        m_odom_broadcaster.sendTransform(odom_trans);

        //next publish the odometry message over ROS
        nav_msgs::Odometry odom;
        odom.header.stamp = now;
        odom.header.frame_id = "odom";

        odom.pose.pose.position.x = m_xloc;
        odom.pose.pose.position.y = m_yloc;
        odom.pose.pose.position.z = 0.0;
        odom.pose.pose.orientation = odom_quat;

        //set the velocity
        odom.child_frame_id = "base_link";
        odom.twist.twist.linear.x =  update_x;
        odom.twist.twist.linear.y =  update_y;
        odom.twist.twist.angular.z =  update_th;

        //publish the message
        m_odompub.publish(odom);
    }

}


void PrydeEncoder::publishEncoderReadings ( void )
{
    bool flag;

    {
        boost::mutex::scoped_lock l ( m_monitorMutex );
        flag = m_exitFlag;
    }

    while(!flag)
    {
        sleep ( SAMPLING_PERIOD );
        {
            boost::mutex::scoped_lock l ( m_monitorMutex );
            flag = m_exitFlag;
        }
        ros::Time now = ros::Time::now(); 
        sample();




        //Prepare the ROS message to publish
        m_encoderTicks.header.stamp = now;
        m_encoderTicks.header.frame_id = "base_link";
        m_encoderTicks.header.seq = m_sampleid;
        m_encoderTicks.ticks.x= m_pubSample.Count[LEFT_REF];
        m_encoderTicks.ticks.y= m_pubSample.Count[RIGHT_REF];
        m_encoderTicks.ticks.z= 0;

        m_encpub.publish(m_encoderTicks);
    }

	std::cout << "Exiting PrydeEncoder::publishEncoderReadings" << std::endl;
}
 


void PrydeEncoder::sample(void)
{

    USB4_FIFOBufferRecord samples[SAMPLES_TO_COLLECT];

    int samplesCollected = 0;
    int prevousSampleCount = 0;

    int bufferCount = 0;
    unsigned char encoderChannels = 0;
    unsigned int timeDifference = 0;

    // 0 = Ignore, 1 = Rising, 2 = Falling, 3 = Change, 4 = High, 5 = Low. 6 = Always, 7 = Always
    unsigned char trigger1Array[8] = {3,0,0,0,0,0,0,0}; // Condition1: Start acquisition on change of input bit 0.  
    unsigned char trigger2Array[8] = {6,6,6,6,6,6,6,6}; // Condition2: Set to always for all input bits.
                                                        
    unsigned char trigger1And = 0;                      // 0 = OR trigger condition for all inputs, 
                                                        // 1 = AND trigger conditions for all inputs
    unsigned char trigger2And = 0;                      // 0 = OR qualifier conditions for inputs
                                                        // 1 = AND

    unsigned char  adcTriggerArray[4] = {0};
    unsigned short adcThreshold[4] = {0};
    unsigned char  pwmTriggerArray[4] = {0};
    unsigned int  pwmThresholdArray[4] = {0};


    // Step 2: Enable capture, quadrature mode to X1, counter mode to 24 bit counter, and enable the counters.
    int failedEncoder = 0;
    for (int i = 0; i < USB4_MAX_ENCODERS; i++)
    {
        m_usbresult = USB4_SetControlMode(deviceNumber, i, 0x844000);
        if(m_usbresult != 0)
        {
            printf("Failed to set the control mode for encoder %d!\n", i);
            failedEncoder = i;
            break;
        }
    }
    
    if (m_usbresult == USB4_SUCCESS)
    {
        // Step 3: Set the sampling period. reg.#30 (n+1 * 2µs) 
        // X = number of seconds desired. N = value to place in register 30.
        // N = ((X * 10^6) / 2) -1
        // 49999 = (.1 * 1000000) / 2 - 1 
        // The value of N for an aproximate 100 ms sampling period would be 49999.
        m_usbresult = USB4_SetSamplingRateMultiplier(deviceNumber, 49999);
        if (m_usbresult != 0)
        {
            printf("Failed to set the sampling rate multiplier%d!\n", failedEncoder);
        }
    }
    
    if (m_usbresult == USB4_SUCCESS)
    {
        //printf("Samples To Collect: %d\n", SAMPLES_TO_COLLECT);

        trigger1Array[0] = 6;   
        trigger1Array[1] = 6;   
        trigger1Array[2] = 6;
        trigger1Array[3] = 6;   
        trigger1Array[4] = 6;   
        trigger1Array[5] = 6;   
        trigger1Array[6] = 6;   
        trigger1Array[7] = 6;   
             
        // Note: No error checking occurs from this point on

        // Step 4: Select the condition for triggering and storage qualification 
        // and number of samples to be collected. reg.#41, reg.#42, reg.#43
        // Trigger1 condition set to Always for all input, 
        // Trigger2 condition set to Always for all input.
        // Trigger1 condition for all bits are OR'd together and Trigger2 conditions 
        // for all bits are OR'd together.
        // See intialization of variables above.
        m_usbresult = USB4_SetTimeBasedLogSettings(deviceNumber, 
                                                   trigger1Array, trigger1And, 
                                                   trigger2Array, trigger2And, 
                                                   adcTriggerArray, adcThreshold,
                                                   pwmTriggerArray, pwmThresholdArray,
                                                   encoderChannels, SAMPLES_TO_COLLECT);

         // Step 5: Clear the FIFO buffer. reg.#38
         m_usbresult = USB4_ClearFIFOBuffer(deviceNumber);

         // Step 6: Enable FIFO. reg.#37
         m_usbresult = USB4_EnableFIFOBuffer(deviceNumber);

         // Step 7: Start acquisition. reg.#45
         m_usbresult = USB4_StartAcquisition(deviceNumber);

         // Step 8: Read data from the FIFO until the specified number of records are collected.
         

         unsigned long timeout = 0;
         while (samplesCollected < SAMPLES_TO_COLLECT && (m_usbresult == 0 || m_usbresult == FIFO_BUFFER_EMPTY))
         {
             bufferCount = SAMPLES_TO_COLLECT - samplesCollected;
             m_usbresult = USB4_ReadFIFOBufferStruct(deviceNumber, &bufferCount, &samples[samplesCollected], timeout);
             samplesCollected += bufferCount;
                
             if (prevousSampleCount != samplesCollected)
             {
                 //printf("\rSamples Collected: %d\n", samplesCollected);
                 //fflush(stdout);
             }

             prevousSampleCount = samplesCollected;
         }

		 ++m_sampleid;

         //printf("\n\n%10s%10s%10s%10s%10s%25s\n", "TimeStamp", "Count0", "Count1", "Count2", "Input", "TimeStamp Differrence");
         //printf("(unit: 21ns)                                       (unit: 21ns) (unit: sec)\n");
         //printf("---------------------------------------------------------------------------\n");
             
         // Step 9: Display the collected sample data.
         for (int i = 0; i < samplesCollected; i++) 
         {
             if (i > 0)
             {
                 timeDifference = samples[i].Time - samples[i-1].Time;
             }
			
			 /*
             printf("%10u%10u%10u%10u%10u%10d\t%f\n", samples[i].Time, samples[i].Count[0], samples[i].Count[1], samples[i].Count[2], 
                       samples[i].Input, timeDifference, timeDifference * 0.000000021);
              */
             m_pubSample.Time = samples[0].Time;
             m_pubSample.Count[LEFT_REF] =  samples[0].Count[LEFT_REF];
             m_pubSample.Count[RIGHT_REF] =  samples[0].Count[RIGHT_REF]; 
			 //logfile << m_sampleid << "," << now << "," << samples[i].Time << "," << samples[i].Count[LEFT_REF] << "," << samples[i].Count[RIGHT_REF] << std::endl << std::flush;
         }
    }
}


int main(int argc, char** argv) 
{
    ros::init(argc, argv, "pose_logger");
    PrydeEncoder encoder_node;	
    
    encoder_node.start();
    return 0;
}

