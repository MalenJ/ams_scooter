#include <arf_encoder/pose_est.hpp>


using namespace std;
using namespace CAS_IRTWalkerFramework;


poseEstimator::poseEstimator() : m_exitFlag(false), m_imuReady(false), m_encReady(false)
{	

    std::cout << "poseEstimator constructor" << std::endl;
}
 
//Destructor
poseEstimator::~poseEstimator()
{
    {
        boost::mutex::scoped_lock l ( m_monitorMutex );
        m_exitFlag = true;
    }

    //Shutting down the main thread
    m_monitorThread.interrupt();
    m_monitorThread.join();

    std::cout << "poseEstimator destructor" << std::endl;
}

void poseEstimator::start(void) 
{ 
    m_monitorThread = boost::thread ( boost::bind ( &poseEstimator::processSensorRedings, this ) );

    ros::Subscriber encoder_sub = m_nh.subscribe("/encoder_ticks", 10, &poseEstimator::encoderCallback,this);
    ros::Subscriber imu_sub = m_nh.subscribe("/casimu/imureal", 10, &poseEstimator::imuCallback,this);
 
    ros::spin();
    std::cout << "Exiting ros::spin" << std::endl;
    
    {
        
        boost::mutex::scoped_lock l ( m_monitorMutex );
        m_exitFlag=false;
    }
}

void poseEstimator::encoderCallback(const boost::shared_ptr<arf_msgs::encoder>& encmsg) 
{
    
    {
        boost::mutex::scoped_lock l ( m_monitorMutex );
        if (!m_encReady)
        {
            m_encmsg=encmsg;
            m_encReady=true;
        }
    }

    //ROS_WARN("Received [%f %f %f %f ]", now, encmsg->ticks.x, encmsg->ticks.y, encmsg->ticks.z);
    
}

void poseEstimator::imuCallback(const boost::shared_ptr<arf_msgs::imuReal>& imumsg) 
{
    //double now = ros::Time::now().toSec(); 

    //ROS_WARN("Received [%f %f %f %f ]", now, encmsg->ticks.x, encmsg->ticks.y, encmsg->ticks.z);
    //ROS_WARN("IMU msg [%f ]", now);
    

    {
        boost::mutex::scoped_lock l ( m_monitorMutex );
        if ((!m_imuReady) && (m_encReady))
        {
            m_imumsg=imumsg;
            m_imuReady=true;
        }
    }
    
}

void poseEstimator::processSensorRedings ( void )
{
    bool flag;

    {
        boost::mutex::scoped_lock l ( m_monitorMutex );
        flag = m_exitFlag;
    }

    while(!flag)
    {
        usleep ( 1000 );
        {
            boost::mutex::scoped_lock l ( m_monitorMutex );
            flag = m_exitFlag;

            if(!flag)
            {
                if((m_imuReady) && (m_encReady))
                {
                    ROS_WARN("IMU [%f %f %f %f]", m_imumsg->header.stamp.toSec(), m_imumsg->RPY.x, m_imumsg->RPY.y, m_imumsg->RPY.z);
                    ROS_WARN("ENC [%f %f %f %f ]\n", m_encmsg->header.stamp.toSec(), m_encmsg->ticks.x, m_encmsg->ticks.y, m_encmsg->ticks.z);
                    m_imuReady=false;
                    m_encReady=false;
                    m_imumsg.reset();
                    m_encmsg.reset();
                }

            }
        }
    }
}

int main(int argc, char** argv) 
{
    ros::init(argc, argv, "pose_logger");
    poseEstimator node;	
    
    node.start();
    return 0;
}

