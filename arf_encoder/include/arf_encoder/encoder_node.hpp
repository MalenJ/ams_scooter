#ifndef __PRYDE_ENCODER_INCLUDED__
#define __PRYDE_ENCODER_INCLUDED__

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>

#include <tf/transform_broadcaster.h>
#include "tf/transform_listener.h"
#include "tf/message_filter.h"
#include <arf_msgs/encoder.h>
#include <boost/thread.hpp>
#include <boost/format.hpp>
#include "libusdusb4.h"
#include <climits>
#include <fstream>
#include <unistd.h>

namespace CAS_IRTWalkerFramework
{


const int SAMPLES_TO_COLLECT = 1; 
const int SAMPLING_PERIOD = 1000; //microseconds
const int LEFT_REF = 0;
const int RIGHT_REF = 1;

const float LDISTPERCOUNT=0.06608284615932709; //mm per tick count
const float RDISTPERCOUNT=0.06603486193757895; //mm per tick count
const float WHEELBASE = (47.5/100.0); //53.5 cm from lefet encoder wheel to right encoder wheel
const float mean_dist_pertick = (LDISTPERCOUNT+RDISTPERCOUNT)/2.0;
const float asymmetry=(RDISTPERCOUNT-LDISTPERCOUNT)/2.0;

class PrydeEncoder
{

public:
    PrydeEncoder();
    virtual ~PrydeEncoder();

	void start(void);

    void encoderCallback(const arf_msgs::encoder& encmsg) ;

private:
    void publishEncoderReadings ( void );

    void sample(void);

    //Handler to ROS core
    ros::NodeHandle m_nh;

    //ROS Publisher
    ros::Publisher  m_encpub;	
    ros::Publisher  m_odompub;
    tf::TransformBroadcaster m_odom_broadcaster;

    ros::Subscriber m_encsub;

	boost::thread  m_monitorThread;
    boost::mutex   m_monitorMutex;	

	//Indicate to stop the nodelet
    bool m_exitFlag;

    short deviceNumber;  // Used to select the device we want to use.
    short deviceCount; // Used to get the number of USB4 devices found.
    int   m_usbresult;

	unsigned long m_sampleid;
    //Logfile
	std::ofstream logfile;

    USB4_FIFOBufferRecord m_pubSample;

    unsigned long m_prevLeftEncoderCount; 
    unsigned long m_prevRightEncoderCount; 

    double m_xloc;
    double m_yloc;
    double m_theta;

    ros::Time     m_lastsampletime;

    arf_msgs::encoder m_encoderTicks;
};

} //namespace CAS_IRTWalkerFramework

#endif
