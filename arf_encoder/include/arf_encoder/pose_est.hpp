#ifndef __POSE_EXTIMATOR_INCLUDED__
#define __POSE_EXTIMATOR_INCLUDED__

#include <ros/ros.h>
#include <ros/console.h>


#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>

#include <arf_msgs/imuRaw.h>
#include <arf_msgs/imuReal.h>
#include <arf_msgs/encoder.h>

#include <nav_msgs/Odometry.h>
#include <boost/thread.hpp>
#include <boost/format.hpp>





namespace CAS_IRTWalkerFramework
{


const int SAMPLES_TO_COLLECT = 1; 
const int SAMPLING_PERIOD = 1; //seconds
const int LEFT_REF = 0;
const int RIGHT_REF = 1;

const float LDISTPERCOUNT=1.3361; //mm per tick count
const float RDISTPERCOUNT=1.3528; //mm per tick count
const float WHEELBASE = (53.5/100.0); //53.5 cm from lefet encoder wheel to right encoder wheel

class poseEstimator
{

public:
    poseEstimator();
    virtual ~poseEstimator();

	void start(void);

    void encoderCallback(const boost::shared_ptr<arf_msgs::encoder>& encmsg);
    void imuCallback(const boost::shared_ptr<arf_msgs::imuReal>& imumsg);
private:

    void processSensorRedings ( void );


    boost::thread  m_monitorThread;
    boost::mutex   m_monitorMutex;	


    //Handler to ROS core
    ros::NodeHandle m_nh;
    ros::NodeHandle m_privateNh;

    boost::shared_ptr<arf_msgs::imuReal> m_imumsg;
    boost::shared_ptr<arf_msgs::encoder> m_encmsg;

    bool m_exitFlag;
    bool m_imuReady;
    bool m_encReady;
};

} //namespace CAS_IRTWalkerFramework

#endif
