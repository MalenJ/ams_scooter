#ifndef __PRYDE_ENCODER_INCLUDED__
#define __PRYDE_ENCODER_INCLUDED__

#include <ros/ros.h>
#include <nodelet/nodelet.h>

#include <boost/thread.hpp>
#include <boost/format.hpp>

#include <string>
#include <unistd.h>


namespace CAS_IRTWalkerFramework
{


const int NUMBER_OF_SAMPLES = 1;

class PrydeEncoder : public nodelet::Nodelet
{
public:
	//Constructor
    PrydeEncoder();

	//Destructor
    virtual ~PrydeEncoder();

private:
    //Private Methods
	//ROS nodelet initialization callback
    virtual void onInit();

    //A thread entry from the Nodelet manager
    void OnInitImpl ( void );

	//Thread entry to send encoder reading updates to the ROS space
    void publishEncoderReadings ( void );

	//Private Members
    
	//Handler to ROS core
    ros::NodeHandle m_nh ;

	//ROS Publisher
	ros::Publisher  m_encpub;	

	//Status update thread: generates status update messages to arf_manager
    boost::thread  m_monitorThread;
	boost::thread  m_initThread;
    boost::mutex   m_monitorMutex;	

    //Indicate to stop the nodelet
    bool m_exitFlag;
};

} //namespace CAS_IRTWalkerFramework

#endif
