#!/usr/bin/env python

import roslib; roslib.load_manifest('ams_ekf')
import rospy
import copy
import time
import os, sys
import subprocess 
import signal
import re
from array import *
import numpy as np
from numpy.linalg import multi_dot
from numpy.linalg import inv
from numpy.linalg import cond
import scipy.io #For matlab
from decimal import Decimal
from scipy.interpolate import interp1d
from scipy.interpolate import InterpolatedUnivariateSpline

from std_msgs.msg import String
from sensor_msgs.msg import NavSatFix
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from arf_msgs.msg import encoder
from darknet_ros_msgs.msg import BoundingBoxes
from geometry_msgs.msg import PoseWithCovarianceStamped 
import tf as tf
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray 
from arf_msgs.msg import imuReal
from ams_ekf import predict 
from ams_ekf import observe 
from ams_ekf import henc
from ams_ekf import pubs

from threading import Thread, Lock

mutex = Lock()


#Global vars
rospy.init_node('ams_ekf_v1', anonymous=True,log_level=rospy.DEBUG)
pose_list=np.array([])
mat= scipy.io.loadmat('/mnt/xavierSSD/git/ams_scooter/ams_ekf/src/demomaploop12.mat')
#mat= scipy.io.loadmat('/home/maleen/git/ams_primary/mapping/2019-03-22-Wentworth/2019-03-22-Wentworth-P4-enumap.mat')
#mat= scipy.io.loadmat('/mnt/xavierSSD/2019-03-22-Wentworth-P4-enumap.mat')
GTmap=mat['finmap']

#Encoder
enc_time=np.array([])
#enc_v=np.array([])
encoder_x=np.array([])
encoder_y=np.array([])
encoder_mean=np.array([])
encXcalib=6.608284615932709e-05
encYcalib=6.603486193757895e-05
calib=np.mean([encXcalib,encYcalib])

#IMU
imu_time=np.array([])
imu_angvZ=np.array([])


#Find nearest
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx
  
class robot_pose:

   def __init__(self,time=0, X=0, Y=0, Phi=0, P=np.zeros([3,3])):

     self.time=time
     self.X=X
     self.Y=Y
     self.Phi=Phi
     self.P=P
     
class observation:

  def __init__(self, img_time, dimensions, frame, labels):
     
    self.img_time=img_time
    self.dimensions=dimensions
    self.labels=labels
    self.ID=np.zeros(labels.size)
        
    h_fov=np.deg2rad(69.4)
    h_width=640
    centerX=h_width/2
    
    rad_per_pix=h_fov/h_width

    centroids=np.zeros((dimensions.shape[0],2))
    rawbearings=np.zeros(dimensions.shape[0])
    gradients=np.zeros(dimensions.shape[0])
    intercepts=np.zeros(dimensions.shape[0])
    
    centroids[:,0]=(dimensions[:,0]+dimensions[:,2])/2
    centroids[:,1]=(dimensions[:,1]+dimensions[:,3])/2
    
    rawbearings=(centroids[:,0]-centerX)*rad_per_pix
    # gradients=np.tan(bearings)
    
    # bx1=(dimensions[:,0]-centerX)*rad_per_pix
    # bx2=(dimensions[:,2]-centerX)*rad_per_pix
    
    if frame=='cam_1_color_optical_frame':       
      self.camera='C1'
      bear=np.pi/4-rawbearings
      gradang=observe.wrapTo2Pi(bear)          
    elif frame=='cam_2_color_optical_frame':
      self.camera='C2'
      bear=np.pi/4+rawbearings
      gradang=observe.wrapTo2Pi(-bear)
    
    self.bearings=gradang
     
  
def imu_cb(imu_msg):
  #print('imu recievd!: ',imu_msg.header.seq)
  global imu_time, imu_angvZ,mutex
  mutex.acquire()
  imu_time=np.append(imu_time,(imu_msg.header.stamp.secs)+(Decimal(imu_msg.header.stamp.nsecs)/1000000000))
  angzmsg=-imu_msg.angular_velocity.z

  if imu_time.size>2:
    #angzmsg=0.5*imu_angvZ[-1]+(1-0.5)*(np.abs(imu_angvZ[-1]-angzmsg))


    if np.abs(imu_angvZ[-1]-angzmsg)<np.deg2rad(0.5):
      #print(np.abs(imu_angvZ[-1]-angzmsg))
      imu_angvZ=np.append(imu_angvZ,0)
    else:
     imu_angvZ=np.append(imu_angvZ,angzmsg)
  else:
      imu_angvZ=np.append(imu_angvZ,angzmsg)

  mutex.release()
  
def encode_cb(enc_msg):
  #print('enc recievd!: ',enc_msg.header.seq)
  
  global enc_time,encoder_x, encoder_y,calib, encoder_mean,pose_list,imu_time,imu_angvZ, mutex, GTmap
  mutex.acquire()
  #print('enc start!')
  U_k=np.zeros(2)
  
  if enc_time.size<1:
      initial_time=(enc_msg.header.stamp.secs)+(Decimal(enc_msg.header.stamp.nsecs)/1000000000)
      initial_P_k=np.array([[0.05,0, 0],[0, 0.05, 0],[0,0,np.radians(5)]])
      initial_pose=robot_pose(initial_time,0,0,0,initial_P_k)
      #print(initial_pose.Phi)
      pose_list=np.append(pose_list,initial_pose)
  
  enc_time=np.append(enc_time,(enc_msg.header.stamp.secs)+(Decimal(enc_msg.header.stamp.nsecs)/1000000000))
  encoder_x=np.append(encoder_x,enc_msg.ticks.x)
  encoder_y=np.append(encoder_y,enc_msg.ticks.y)
  
  encoder_mean=np.append(encoder_mean,henc.handle_dat_encoder(encoder_x,encoder_y,-1))
  
  previous_time=pose_list[-1].time
  current_time=enc_time[-1]
  
  dt=np.float64(current_time-previous_time)
  
  if enc_time.size>2:
    enc_interp=InterpolatedUnivariateSpline(np.float64(enc_time), encoder_mean, k=1)
    U_k[0]=(enc_interp(np.float64(current_time))-enc_interp(np.float64(previous_time)))*calib
  else:
    U_k[0]=(encoder_mean[-1]-encoder_mean[find_nearest(enc_time,previous_time)])*calib
      
  if imu_time.size>2:
    thetaZ_interp=InterpolatedUnivariateSpline(np.float64(imu_time), imu_angvZ, k=1)
    U_k[1]=thetaZ_interp(np.float64(previous_time))
    #imu_indx=find_nearest(imu_time,previous_time)
    #U_k[1]=imu_angvZ[imu_indx]

    #print(imu_time)
    #print(previous_time)
  else:
    U_k[1]=0
    

  pose_k=pose_list[-1]
  
  X_k1k,P_k1k=predict.predict_pose(pose_k,U_k,dt)
  X_k1k[2]=observe.wrapTo2Pi(X_k1k[2])
  
  pose_k1k=robot_pose(current_time,X_k1k[0],X_k1k[1],X_k1k[2],P_k1k)
  
  pose_list=np.append(pose_list,pose_k1k)
  
  pubs.publishpose(pose_list, GTmap)
  #print('enc end!')
  mutex.release()
  
def yolo_cb(yolo_msg):
  #print('yolo recievd!: ',yolo_msg.header.seq)
  global enc_time,pose_list,imu_time,imu_angvZ,encoder_mean,mutex
  if enc_time.size >2:
    mutex.acquire()
    #print('yolo start')
    U_k=np.zeros(2)
    
    lamp_dimensions=np.array([])
    semantic_label=np.array([])
    yolo_time=(yolo_msg.header.stamp.secs)+(Decimal(yolo_msg.header.stamp.nsecs)/1000000000)
    
    image_time=(yolo_msg.image_header.stamp.secs)+(Decimal(yolo_msg.image_header.stamp.nsecs)/1000000000)
    frame=yolo_msg.image_header.frame_id
    
    tree_remover=0
      
    for obj in yolo_msg.bounding_boxes:
          
          #if obj.Class!='tree':
            lamp_dimensions=np.append(lamp_dimensions,[obj.xmin,obj.ymin,obj.xmax,obj.ymax])
            semantic_label=np.append(semantic_label,obj.Class)
            tree_remover=1
            
    if tree_remover==1:
          
      lamp_dimensions=np.reshape(lamp_dimensions, (lamp_dimensions.size/4, 4)) 
      
      obz=observation(image_time,lamp_dimensions,frame,semantic_label)
      
      current_time=image_time
      
      d=pose_list.size
      previous_pose=robot_pose()
      
      x=pose_list.size #Index of
      
      for pose in pose_list[::-1]:
            x=x-1      
            if pose.time<current_time:
                  previous_pose=pose
                  break
                
      previous_time=previous_pose.time
      dt=np.float64(current_time-previous_time)

      enc_diff=enc_time[-1]-current_time

      #Trim arrays
      if enc_time.size>100:
        enc_time=np.delete(enc_time,np.s_[0:5],axis=0)
        encoder_mean=np.delete(encoder_mean,np.s_[0:5],axis=0)
      
      if imu_time.size>100: 
        imu_time=np.delete(imu_time,np.s_[0:5],axis=0)
        imu_angvZ=np.delete(imu_angvZ,np.s_[0:5],axis=0)
        
      enc_interp=InterpolatedUnivariateSpline(np.float64(enc_time), encoder_mean, k=1)
      U_k[0]=(enc_interp(np.float64(current_time))-enc_interp(np.float64(previous_time)))*calib
      



      thetaZ_interp=InterpolatedUnivariateSpline(np.float64(imu_time), imu_angvZ, k=1)
      U_k[1]=thetaZ_interp(np.float64(previous_time))
      
      # enc_indx=find_nearest(enc_time,previous_time)
      #imu_indx=find_nearest(imu_time,previous_time)
      #U_k[1]=imu_angvZ[imu_indx]


      X_k1k,P_k1k=predict.predict_pose(previous_pose,U_k,dt)
      X_k1k[2]=observe.wrapTo2Pi(X_k1k[2])

      X_k1k1,P_k1k1,detects=observe.obs_update(X_k1k, P_k1k, obz, GTmap)
      
      if detects>0:
        corrected_pose=robot_pose(current_time,X_k1k1[0],X_k1k1[1],X_k1k1[2],P_k1k1)
        pose_list=np.append(pose_list,corrected_pose)
      
      # if enc_diff<0:
      #     pose_list=np.append(pose_list,corrected_pose)
      #     print('+current ',current_time)
      #     print('+previous ',previous_time)
      # else:
      
      #   pose_list=np.delete(pose_list,np.s_[0:x],axis=0)
        
      #   pose_list[0]=corrected_pose
        
      #   iter=0
      #   for pose in pose_list[1:]:
              
      #     previous_time=pose_list[iter].time
      #     current_time=pose.time
      #     dt=np.float64(current_time-previous_time)
      #     print('-current ',current_time)
      #     print('-previous ',previous_time)
      #     #U_k[0]=(encoder_mean[find_nearest(enc_time,current_time)]-encoder_mean[find_nearest(enc_time,previous_time)])*calib
      #     # enc_indx=find_nearest(enc_time,previous_time)
      #     # U_k[0]=enc_v[enc_indx]
      #     U_k[0]=(enc_interp(np.float64(current_time))-enc_interp(np.float64(previous_time)))*calib
      #     U_k[1]=thetaZ_interp(np.float64(previous_time))
      #     #imu_indx=find_nearest(imu_time,previous_time)
      #     #U_k[1]=imu_angvZ[imu_indx]
              
      #     X_k1k,P_k1k=predict.predict_pose(pose,U_k,dt)
      #     X_k1k[2]=observe.wrapTo2Pi(X_k1k[2])
        
      #     pose.X=X_k1k[0]
      #     pose.Y=X_k1k[1]
      #     pose.Phi=X_k1k[1]
      #     pose.P=P_k1k
          
      #     iter=iter+1
        
      
      pubs.publishpose(pose_list, GTmap)
    #print('yolo end')
    mutex.release()
      
def main(args):
      
  global enc_time
   
   #Odometry
   #odom_sub=rospy.Subscriber('/odom', Odometry, odom_cb)
  enc_sub=rospy.Subscriber('/encoder_ticks', encoder, encode_cb)
  imu_sub=rospy.Subscriber('/casimu/imureal', imuReal, imu_cb)
  #imu_sub=rospy.Subscriber('/imudata', Imu, imu_cb)
  yolo_sub=rospy.Subscriber('/darknet_ros/bounding_boxes',BoundingBoxes,yolo_cb)
   
  try:
     rospy.spin()
  except KeyboardInterrupt:
     print("Shutting down")
   
 
if __name__ == '__main__':
    main(sys.argv)
