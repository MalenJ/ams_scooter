#!/usr/bin/env python
# Ravindra Ranasinghe @ UTS-CAS

import roslib; roslib.load_manifest('relay_msgs')
import rospy
import copy
import time
import os, sys
import subprocess 
import signal
import re
from array import *
from std_msgs.msg import String
from sensor_msgs.msg import NavSatFix
from mobscooter_msgs.msg import RSSI
from mobscooter_msgs.msg import Cellular
from arf_msgs.msg import encoder
from arf_msgs.msg import imuRaw
from arf_msgs.msg import imuReal

from sensor_msgs.msg import Imu

from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseWithCovarianceStamped 

relay_obj = None

class relay_intf:
        
    'Node to relay WiFi scan, GPS and Cellular tower data'

    # Constructor
    def __init__(self):
        rospy.init_node('relay_intf', anonymous=True, log_level=rospy.DEBUG)		
        self.exit=0

        
        self.odom_message=Odometry()

        #individual message counters

        self.odom_cnt=0

 	#setup a ros publisher

        self.odom_pub=rospy.Publisher('/rtab_odom', Odometry, queue_size=10) 
 

        #setup subscribers

        self.odomsub=rospy.Subscriber('/rtabmap/localization_pose', PoseWithCovarianceStamped, self.rtab_Cb)

      



    #destructor     
    def __del__(self):
        print('Shutting down the relay_intf object')


    def rtab_Cb(self, pose_msg):
        print 'encode_Cb received. imureal msg :', pose_msg

        self.odom_cnt = self.odom_cnt + 1

        self.odom_message.header.stamp = pose_msg.header.stamp
        self.odom_message.header.seq = self.odom_cnt

        self.odom_message.header.frame_id = 'map'
        self.odom_message.pose=pose_msg.pose
        self.odom_message.pose=pose_msg.pose

        self.odom_pub.publish(self.odom_message)


    def exitInd(self):
        self.exit = 1

def signal_handler(signal, frame):
    print('ctrl-c signal')
    relay_obj.exitInd()
           
if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    relay_obj = relay_intf()

    rospy.spin()
    print 'relay_intf Exit()!'
  


         




