#!/usr/bin/env python
# Ravindra Ranasinghe @ UTS-CAS

import roslib; roslib.load_manifest('relay_msgs')
import rospy
import copy
import time
import os, sys
import subprocess 
import signal
import re
from array import *
from std_msgs.msg import String
from sensor_msgs.msg import NavSatFix

#import scipy.io
import message_filters
from decimal import Decimal
import numpy as np
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
import tf as tf
import tf2_ros
from geometry_msgs.msg import TransformStamped 

from sensor_msgs.msg import Image

import cv2
from cv_bridge import CvBridge, CvBridgeError
import glob
import time


#mat= scipy.io.loadmat('/home/maleen/git/ams_scooter/ams_ekf/src/demomaploop12.mat')
        #mat= scipy.io.loadmat('/home/maleen/git/ams_primary/mapping/2019-03-22-Wentworth/2019-03-22-Wentworth-P4-enumap.mat')
        #mat= scipy.io.loadmat('/mnt/xavierSSD/2019-03-22-Wentworth-P4-enumap.mat')
#GTmap=mat['finmap']

relay_obj = None
imu_angvZ=np.array([])
imu_thetaZ=np.array([])
imu_thetaZ=np.array([])
imu_thetaX=np.array([])
imu_thetaY=np.array([])
br = tf2_ros.TransformBroadcaster()


class relay_intf:
        
    'Node to relay WiFi scan, GPS and Cellular tower data'

    # Constructor
    def __init__(self):
        rospy.init_node('relay_intf', anonymous=True, log_level=rospy.DEBUG)		
        self.exit=0

        self.images=[]
        
        #individual message counters
        self.wifimsgs_cnt = 0
        self.celldatamsgs_cnt = 0
        self.gpslocmsgs_cnt = 0
        self.netlocmsgs_cnt = 0
        self.encodermsgs_cnt=0
        self.imurawmsgs_cnt=0
        self.imurealmsgs_cnt=0
        self.tsmsgs_cnt=0

        self.imudata_cnt=0

 	#setup a ros publisher
  

        self.imagepubs=rospy.Publisher('/cam', Image, queue_size=1)
        self.bridge = CvBridge()

        #setup subscribers


   
        
        cnt=0
        
        
        for img in sorted(glob.glob("/mnt/xavierSSD/vidcon/dual/*.jpg")):
            cnt=cnt+1
            cv_img = cv2.imread(img)
            print(img)
            image_message = self.bridge.cv2_to_imgmsg(cv_img, encoding="bgr8")
            self.imagepubs.publish(image_message)
            time.sleep(1) # seconds
        # def read_img(img_list, img):
        #     n = cv2.imread(img, 0)
        #     img_list.append(n)
        #     return img_list

        # path = glob.glob("/home/maleen/vidcon/split/*.jpg") #or jpg
        # list_ = []

        # cv_image = [read_img(list_, img) for img in path]
        
     
        # i=0
        
        # for pic in cv_image:
            
        #     i=i+1
        #     print(type(pic))
        #     time.sleep(1/5) # seconds
            
            # image_message = self.bridge.cv2_to_imgmsg(img, encoding="passthrough")
            
            # self.imagepubs.publish(image_message)



    #destructor     
    def __del__(self):
        print('Shutting down the relay_intf object')
    
    


    


    def exitInd(self):
        self.exit = 1

def signal_handler(signal, frame):
    print('ctrl-c signal')
    relay_obj.exitInd()
           
if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    relay_obj = relay_intf()

    rospy.spin()
    print ('relay_intf Exit()!')
  


         




