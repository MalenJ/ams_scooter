#!/usr/bin/env python
# Ravindra Ranasinghe @ UTS-CAS

import roslib; roslib.load_manifest('relay_msgs')
import rospy
import copy
import time
import os, sys
import subprocess 
import signal
import re
from array import *
from std_msgs.msg import String
from sensor_msgs.msg import NavSatFix
from mobscooter_msgs.msg import RSSI
from mobscooter_msgs.msg import Cellular
from arf_msgs.msg import encoder
from arf_msgs.msg import imuRaw
from arf_msgs.msg import imuReal
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
#import scipy.io
import message_filters
from decimal import Decimal
import numpy as np
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
import tf as tf
import tf2_ros
from geometry_msgs.msg import TransformStamped 
import message_filters

#mat= scipy.io.loadmat('/home/maleen/git/ams_scooter/ams_ekf/src/demomaploop12.mat')
        #mat= scipy.io.loadmat('/home/maleen/git/ams_primary/mapping/2019-03-22-Wentworth/2019-03-22-Wentworth-P4-enumap.mat')
        #mat= scipy.io.loadmat('/mnt/xavierSSD/2019-03-22-Wentworth-P4-enumap.mat')
#GTmap=mat['finmap']


class relay_intf:
        
    'Node to relay imu data intensely'

    # Constructor
    def __init__(self):
        rospy.init_node('relay_intf', anonymous=True, log_level=rospy.DEBUG)		
        self.exit=0

        self.imudata_msg = Imu()
        self.imudata_cnt=0


        self.imu_accel = message_filters.Subscriber('/back_cam/accel/sample',Imu)
        self.imu_gyro = message_filters.Subscriber('/back_cam/gyro/sample', Imu)
        
        self.ats = message_filters.ApproximateTimeSynchronizer([self.imu_accel, self.imu_gyro], queue_size=1, slop=0.1)
        self.ats.registerCallback(self.imuReal_Cb)
      
        self.imudatapub=rospy.Publisher('/imudata_realsense', Imu, queue_size=10)



    #destructor     
    def __del__(self):
        print('Shutting down the relay_intf object')
    
    


    def imuReal_Cb(self, accelmsg,gyromsg):
        
        self.imudata_msg=gyromsg
        self.imudata_cnt = self.imudata_cnt + 1
        self.imudata_msg.header.stamp = rospy.Time.now()
        self.imudata_msg.header.seq = self.imudata_cnt
      

        #self.imudata_msg.angular_velocity.x=gyromsg.angular_velocity.x+0.00349065847695
        #self.imudata_msg.angular_velocity.y=gyromsg.angular_velocity.y
        #self.imudata_msg.angular_velocity.z=gyromsg.angular_velocity.z

        self.imudata_msg.linear_acceleration=accelmsg.linear_acceleration
        self.imudata_msg.linear_acceleration_covariance=accelmsg.linear_acceleration_covariance
        print ('encode_Cb received. imureal msg :', self.imudata_msg)
        self.imudatapub.publish(self.imudata_msg)


    def exitInd(self):
        self.exit = 1

def signal_handler(signal, frame):
    print('ctrl-c signal')
    relay_obj.exitInd()
           
if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    relay_obj = relay_intf()

    rospy.spin()
    print ('relay_intf Exit()!')
  


         




