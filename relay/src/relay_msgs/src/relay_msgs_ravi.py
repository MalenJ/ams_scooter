#!/usr/bin/env python
# Ravindra Ranasinghe @ UTS-CAS

import roslib; roslib.load_manifest('relay_msgs')
import rospy
import copy
import time
import os, sys
import subprocess 
import signal
import re
from array import *
from std_msgs.msg import String
from sensor_msgs.msg import NavSatFix
from mobscooter_msgs.msg import RSSI
from mobscooter_msgs.msg import Cellular
from arf_msgs.msg import encoder
from arf_msgs.msg import imuRaw
from arf_msgs.msg import imuReal
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
#import scipy.io
import message_filters
from decimal import Decimal
import numpy as np
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
import tf as tf
import tf2_ros
from geometry_msgs.msg import TransformStamped 
from sensor_msgs.msg import JointState


#mat= scipy.io.loadmat('/home/maleen/git/ams_scooter/ams_ekf/src/demomaploop12.mat')
        #mat= scipy.io.loadmat('/home/maleen/git/ams_primary/mapping/2019-03-22-Wentworth/2019-03-22-Wentworth-P4-enumap.mat')
        #mat= scipy.io.loadmat('/mnt/xavierSSD/2019-03-22-Wentworth-P4-enumap.mat')
#GTmap=mat['finmap']

class relay_intf:
        
    'Node to relay WiFi scan, GPS and Cellular tower data'

    # Constructor
    def __init__(self):
        rospy.init_node('relay_intf', anonymous=True, log_level=rospy.DEBUG)		
        self.exit=0

        self.odom_message=Odometry()

        #individual message counters

        # self.marker_pub=rospy.Publisher('/mapviz_fin', MarkerArray, queue_size=10) 
        self.odom_pub=rospy.Publisher('/fin_odom', Odometry, queue_size=10) 

        #setup subscribers
        #self.wifisub = rospy.Subscriber('/wifidata', String, self.wifi_Cb)
        #self.celldatasub = rospy.Subscriber('/celldata', String, self.celldata_Cb)
        #self.GPSlocsub = rospy.Subscriber('/GPS_loc', NavSatFix, self.GPSloc_Cb)
        #self.NETlocsub = rospy.Subscriber('/NET_loc', NavSatFix, self.NETloc_Cb)
        #self.encodersub=rospy.Subscriber('/encoder_ticks', encoder)
       	#self.imurawsub = rospy.Subscriber('/casimu/imuraw', imuRaw, self.imuRaw_Cb)
        self.reelsub = rospy.Subscriber('/r2t2_reel/joint_states', JointState, self.Cb)


    #destructor     
    def __del__(self):
        print('Shutting down the relay_intf object')
    
    
        
    def Cb(self, REELMSG):
     	print(REELMSG.position[0])
        # print('imumseq :', imureal_msg.header.seq)
        
        self.odom_message.header = REELMSG.header
        self.odom_message.header.frame_id = 'reel_odom'
    
        self.odom_message.pose.pose.position.x = REELMSG.position[0]/1000
        self.odom_message.twist.twist.linear.x = REELMSG.velocity[0]/1000

           
        self.odom_pub.publish(self.odom_message)


    def exitInd(self):
        self.exit = 1

def signal_handler(signal, frame):
    print('ctrl-c signal')
    relay_obj.exitInd()
           
if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    relay_obj = relay_intf()

    rospy.spin()
    print ('relay_intf Exit()!')
  


         




