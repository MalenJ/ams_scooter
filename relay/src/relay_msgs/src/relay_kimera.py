#!/usr/bin/env python
# Ravindra Ranasinghe @ UTS-CAS

import roslib; roslib.load_manifest('relay_msgs')
import rospy
import copy
import time
import os, sys
import subprocess 
import signal
import re
from array import *
from std_msgs.msg import String
from sensor_msgs.msg import NavSatFix
from mobscooter_msgs.msg import RSSI
from mobscooter_msgs.msg import Cellular
from arf_msgs.msg import encoder
from arf_msgs.msg import imuRaw
from arf_msgs.msg import imuReal
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
#import scipy.io
import message_filters
from decimal import Decimal
import numpy as np
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
import tf as tf
import tf2_ros
from geometry_msgs.msg import TransformStamped 

#mat= scipy.io.loadmat('/home/maleen/git/ams_scooter/ams_ekf/src/demomaploop12.mat')
        #mat= scipy.io.loadmat('/home/maleen/git/ams_primary/mapping/2019-03-22-Wentworth/2019-03-22-Wentworth-P4-enumap.mat')
        #mat= scipy.io.loadmat('/mnt/xavierSSD/2019-03-22-Wentworth-P4-enumap.mat')
#GTmap=mat['finmap']

relay_obj = None
imu_angvZ=np.array([])
imu_thetaZ=np.array([])
imu_thetaZ=np.array([])
imu_thetaX=np.array([])
imu_thetaY=np.array([])
br = tf2_ros.TransformBroadcaster()


class relay_intf:
        
    'Node to relay WiFi scan, GPS and Cellular tower data'

    # Constructor
    def __init__(self):
        rospy.init_node('relay_intf', anonymous=True, log_level=rospy.DEBUG)		
        self.exit=0

        self.imudata_msg = Imu()
        self.markerarray=MarkerArray()
        self.marker=Marker()
        self.ts_message=Odometry()
        self.t =TransformStamped()
        
        #individual message counters
        self.wifimsgs_cnt = 0
        self.celldatamsgs_cnt = 0
        self.gpslocmsgs_cnt = 0
        self.netlocmsgs_cnt = 0
        self.encodermsgs_cnt=0
        self.imurawmsgs_cnt=0
        self.imurealmsgs_cnt=0
        self.tsmsgs_cnt=0

        self.imudata_cnt=0

 	#setup a ros publisher
  

        self.imudatapub=rospy.Publisher('/imudata_realsense', Imu, queue_size=10)
      

        #setup subscribers


        self.imurealsubori = rospy.Subscriber('/back_cam/imu', Imu, self.imuReal_Cb)

        

      



    #destructor     
    def __del__(self):
        print('Shutting down the relay_intf object')
    
    


    def imuReal_Cb(self, imureal_msg):
        #print ('encode_Cb received. imureal msg :', imureal_msg)
        self.imudata_msg=imureal_msg.header
        
        self.imudata_cnt = self.imudata_cnt + 1
        self.imudata_msg.header.stamp = rospy.Time.now()
        self.imudata_msg.header.seq = self.imudata_cnt



        self.imudatapub.publish(self.imudata_msg)


    def exitInd(self):
        self.exit = 1

def signal_handler(signal, frame):
    print('ctrl-c signal')
    relay_obj.exitInd()
           
if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    relay_obj = relay_intf()

    rospy.spin()
    print ('relay_intf Exit()!')
  


         




