#!/usr/bin/env python
# Ravindra Ranasinghe @ UTS-CAS

import roslib; roslib.load_manifest('relay_msgs')
import rospy
import copy
import time
import os, sys
import subprocess 
import signal
import re
from array import *
from std_msgs.msg import String
from sensor_msgs.msg import NavSatFix
from mobscooter_msgs.msg import RSSI
from mobscooter_msgs.msg import Cellular
from arf_msgs.msg import encoder
from arf_msgs.msg import imuRaw
from arf_msgs.msg import imuReal
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
#import scipy.io
import message_filters
from decimal import Decimal
import numpy as np
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
import tf as tf
import tf2_ros
from geometry_msgs.msg import TransformStamped 
from sensor_msgs import JointState

#mat= scipy.io.loadmat('/home/maleen/git/ams_scooter/ams_ekf/src/demomaploop12.mat')
        #mat= scipy.io.loadmat('/home/maleen/git/ams_primary/mapping/2019-03-22-Wentworth/2019-03-22-Wentworth-P4-enumap.mat')
        #mat= scipy.io.loadmat('/mnt/xavierSSD/2019-03-22-Wentworth-P4-enumap.mat')
#GTmap=mat['finmap']

relay_obj = None
imu_angvZ=np.array([])
imu_thetaZ=np.array([])
imu_thetaZ=np.array([])
imu_thetaX=np.array([])
imu_thetaY=np.array([])
br = tf2_ros.TransformBroadcaster()


class relay_intf:
        
    'Node to relay WiFi scan, GPS and Cellular tower data'

    # Constructor
    def __init__(self):
        rospy.init_node('relay_intf', anonymous=True, log_level=rospy.DEBUG)		
        self.exit=0

        self.imudata_msg = Imu()
        self.markerarray=MarkerArray()
        self.marker=Marker()
        self.ts_message=Odometry()
        self.t =TransformStamped()
        
        #individual message counters
        self.wifimsgs_cnt = 0
        self.celldatamsgs_cnt = 0
        self.gpslocmsgs_cnt = 0
        self.netlocmsgs_cnt = 0
        self.encodermsgs_cnt=0
        self.imurawmsgs_cnt=0
        self.imurealmsgs_cnt=0
        self.tsmsgs_cnt=0

        self.imudata_cnt=0

 	#setup a ros publisher
        # self.wifipub = rospy.Publisher('/wifidata_ts', RSSI , queue_size=10)
        # self.celldatapub = rospy.Publisher('/celldata_ts', Cellular , queue_size=10)
        # self.GPSlocpub = rospy.Publisher('/GPS_loc_ts', NavSatFix , queue_size=10)
        # self.NETlocpub = rospy.Publisher('/NET_loc_ts', NavSatFix , queue_size=10)
        # self.encoderpub=rospy.Publisher('/encoder_ticks_ts', encoder, queue_size=10)
        # self.imurawpub = rospy.Publisher('/imuraw_ts', imuRaw , queue_size=10)
        # self.imurealpub=rospy.Publisher('/imureal_ts', imuReal, queue_size=10)

        self.imudatapub=rospy.Publisher('/imudata', Imu, queue_size=10)
        # self.marker_pub=rospy.Publisher('/mapviz_fin', MarkerArray, queue_size=10) 
        self.tsodom_pub=rospy.Publisher('/ts_odom', Odometry, queue_size=10) 

        #setup subscribers
        #self.wifisub = rospy.Subscriber('/wifidata', String, self.wifi_Cb)
        #self.celldatasub = rospy.Subscriber('/celldata', String, self.celldata_Cb)
        #self.GPSlocsub = rospy.Subscriber('/GPS_loc', NavSatFix, self.GPSloc_Cb)
        #self.NETlocsub = rospy.Subscriber('/NET_loc', NavSatFix, self.NETloc_Cb)
        #self.encodersub=rospy.Subscriber('/encoder_ticks', encoder)
       	#self.imurawsub = rospy.Subscriber('/casimu/imuraw', imuRaw, self.imuRaw_Cb)
        self.imurealsubori = rospy.Subscriber('/casimu/imureal', imuReal, self.imuReal_Cb)
        self.imurealsub=message_filters.Subscriber('/casimu/imureal', imuReal)
        self.odomsub=message_filters.Subscriber('/odom', Odometry)
        
        self.ts = message_filters.ApproximateTimeSynchronizer([self.odomsub, self.imurealsub], queue_size=10, slop=0.02)
        self.ts.registerCallback(self.tscallback)

      



    #destructor     
    def __del__(self):
        print('Shutting down the relay_intf object')
    
    
    def wifi_Cb(self, wifi_msg):
        print ('wifi_Cb received. wifi scan :', wifi_msg.data)

        wifi_msg_mod = RSSI()

        self.wifimsgs_cnt = self.wifimsgs_cnt + 1
        wifi_msg_mod.header.stamp = rospy.Time.now()
        wifi_msg_mod.header.seq = self.wifimsgs_cnt
        wifi_msg_mod.header.frame_id = 'rssi_data'
        wifi_msg_mod.scan = wifi_msg.data
        #print self.wifi_msg_mod
        self.wifipub.publish(wifi_msg_mod)


    def celldata_Cb(self, celldata_msg):
        print ('celldata_Cb received. celldata msg :', celldata_msg.data)
        
        cellular_msg_mod = Cellular()

        self.celldatamsgs_cnt = self.celldatamsgs_cnt + 1
        cellular_msg_mod.header.stamp = rospy.Time.now()
        cellular_msg_mod.header.seq = self.celldatamsgs_cnt
        cellular_msg_mod.header.frame_id = 'cellular_data'
        cellular_msg_mod.cellscan = celldata_msg.data
        self.celldatapub.publish(cellular_msg_mod)


    def GPSloc_Cb(self, gpsloc_msg):
        print ('GPSloc_Cb received. celldata msg :', gpsloc_msg)

        self.gpslocmsgs_cnt = self.gpslocmsgs_cnt + 1
        gpsloc_msg.header.stamp = rospy.Time.now()
        gpsloc_msg.header.seq = self.gpslocmsgs_cnt
        gpsloc_msg.header.frame_id = 'GPSloc_data'
        self.GPSlocpub.publish(gpsloc_msg)

    def NETloc_Cb(self, netloc_msg):
        print('NETloc_Cb received. celldata msg :', netloc_msg)

        self.netlocmsgs_cnt = self.netlocmsgs_cnt + 1
        netloc_msg.header.stamp = rospy.Time.now()
        netloc_msg.header.seq = self.netlocmsgs_cnt
        netloc_msg.header.frame_id = 'NETloc_data'
        self.NETlocpub.publish(netloc_msg)
        
    def tscallback(self, odom_msg,imureal_msg):
     	# print ('odom time :',(odom_msg.header.stamp.secs)+(Decimal(odom_msg.header.stamp.nsecs)/1000000000)-((imureal_msg.header.stamp.secs)+(Decimal(imureal_msg.header.stamp.nsecs)/1000000000)))
        # print ('imutime :', (imureal_msg.header.stamp.secs)+(Decimal(imureal_msg.header.stamp.nsecs)/1000000000))
        # print ('rospy now :', (rospy.Time.now().secs)+(Decimal(rospy.Time.now().nsecs)/1000000000)-((odom_msg.header.stamp.secs)+(Decimal(odom_msg.header.stamp.nsecs)/1000000000)))
        global imu_angvZ,imu_thetaZ,imu_thetaX,imu_thetaY,br
        
        # print('odomseq :', odom_msg.header.seq)
        # print('imumseq :', imureal_msg.header.seq)
        self.tsmsgs_cnt = self.tsmsgs_cnt + 1
        self.ts_message.header.stamp = rospy.Time.now()
        self.ts_message.header.seq = self.tsmsgs_cnt
        self.ts_message.header.frame_id = 'odom_ts'
        self.ts_message.child_frame_id = "base_link"
        self.ts_message.pose=odom_msg.pose
        self.ts_message.twist=odom_msg.twist
    
        
        if imu_angvZ.size<(10*60*3):
            imu_angvZ=np.append(imu_angvZ,imureal_msg.angular_velocity.z)
            imu_thetaZ=np.append(imu_thetaZ,imureal_msg.RPY.z)
            imu_thetaX=np.append(imu_thetaX,imureal_msg.RPY.x)
            imu_thetaY=np.append(imu_thetaY,imureal_msg.RPY.y)
            quaternion = tf.transformations.quaternion_from_euler(imureal_msg.RPY.x, imureal_msg.RPY.y, imureal_msg.RPY.z)
            self.ts_message.pose.pose.orientation.x = quaternion[0]
            self.ts_message.pose.pose.orientation.y = quaternion[1]
            self.ts_message.pose.pose.orientation.z = quaternion[2]
            self.ts_message.pose.pose.orientation.w = quaternion[3]
            
            self.ts_message.twist.twist.angular.z=imureal_msg.angular_velocity.z    
        else:
            quaternion = tf.transformations.quaternion_from_euler(imureal_msg.RPY.x-np.mean(imu_thetaX), imureal_msg.RPY.y-np.mean(imu_thetaY), imureal_msg.RPY.z-np.mean(imu_thetaZ))
            self.ts_message.pose.pose.orientation.x = quaternion[0]
            self.ts_message.pose.pose.orientation.y = quaternion[1]
            self.ts_message.pose.pose.orientation.z = quaternion[2]
            self.ts_message.pose.pose.orientation.w = quaternion[3]
            
            self.ts_message.twist.twist.angular.z=imureal_msg.angular_velocity.z-np.mean(imu_angvZ) 
            print('Ready to go :',imureal_msg.RPY.z-np.mean(imu_thetaZ))
                        
            ##TF

        
        self.t.header.stamp =  self.ts_message.header.stamp
        self.t.header.frame_id = "odom_ts"
        self.t.child_frame_id = "base_link2"
        self.t.transform.translation.x = self.ts_message.pose.pose.position.x
        self.t.transform.translation.y = self.ts_message.pose.pose.position.y
        self.t.transform.translation.z = 0.0
        self.t.transform.rotation.x = self.ts_message.pose.pose.orientation.x
        self.t.transform.rotation.y = self.ts_message.pose.pose.orientation.y
        self.t.transform.rotation.z = self.ts_message.pose.pose.orientation.z
        self.t.transform.rotation.w = self.ts_message.pose.pose.orientation.w      
        
        
        self.tsodom_pub.publish(self.ts_message)
        br.sendTransform(self.t)


    # def encode_Cb(self, encoder_msg):
    #  	print ('encode_Cb received. encoder msg :')
    #     global GTmap
    #     #self.encodermsgs_cnt = self.encodermsgs_cnt + 1
    #     #encoder_msg.header.stamp = rospy.Time.now()
    #     #encoder_msg.header.seq = self.encodermsgs_cnt
    #     #encoder_msg.header.frame_id = 'encoder_data'
    #     #self.encoderpub.publish(encoder_msg)

          
    #     for m in range(0,GTmap.shape[1]):
    #         self.marker.header.frame_id = 'mapvizual'
    #         self.marker.header.stamp=rospy.Time.now()
    #         self.marker.pose.position.x=GTmap[0,m]
    #         self.marker.pose.position.y=GTmap[1,m]
    #         self.marker.pose.position.z=0
    #         self.marker.type = self.marker.SPHERE
    #         self.marker.scale.x = 0.2
    #         self.marker.scale.y = 0.2
    #         self.marker.scale.z = 0.2
    #         self.marker.color.a = 1
    #         self.marker.color.r = 1.0
    #         self.marker.color.g = 0
    #         self.marker.color.b = 0.0
    #         self.marker.lifetime=rospy.Duration(0)
    #         self.marker.id=m
    #         self.markerarray.markers.append(self.marker)
            
    #         self.marker_pub.publish(self.markerarray)


    def imuRaw_Cb(self, imuraw_msg):
     	print ('imuRaw_Cb received. imuraw msg :', imuraw_msg)

        self.imurawmsgs_cnt = self.imurawmsgs_cnt + 1
        imuraw_msg.header.stamp = rospy.Time.now()
        imuraw_msg.header.seq = self.imurawmsgs_cnt
        imuraw_msg.header.frame_id = 'imuraw_data'
        self.imurawpub.publish(imuraw_msg)


    # def imuReal_Cb(self, imureal_msg):
    #  	#print ('encode_Cb received. imureal msg :', imureal_msg)

    #     self.imurealmsgs_cnt = self.imurealmsgs_cnt + 1
    #     imureal_msg.header.stamp = rospy.Time.now()
    #     imureal_msg.header.seq = self.imurealmsgs_cnt
    #     imureal_msg.header.frame_id = 'imureal_data'
    #     self.imurealpub.publish(imureal_msg)


    def imuReal_Cb(self, imureal_msg):
        #print ('encode_Cb received. imureal msg :', imureal_msg)

        self.imudata_cnt = self.imudata_cnt + 1
        self.imudata_msg.header.stamp = imureal_msg.header.stamp
        self.imudata_msg.header.seq = self.imudata_cnt
        self.imudata_msg.header.frame_id = 'imu_data'
        self.imudata_msg.orientation=imureal_msg.orientation
        self.imudata_msg.angular_velocity=imureal_msg.angular_velocity
        self.imudata_msg.linear_acceleration=imureal_msg.linear_acceleration

        self.imudatapub.publish(self.imudata_msg)


    def exitInd(self):
        self.exit = 1

def signal_handler(signal, frame):
    print('ctrl-c signal')
    relay_obj.exitInd()
           
if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    relay_obj = relay_intf()

    rospy.spin()
    print ('relay_intf Exit()!')
  


         




