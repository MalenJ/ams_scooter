package mobscooter_msgs;

public interface RSSI extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "mobscooter_msgs/RSSI";
  static final java.lang.String _DEFINITION = "std_msgs/Header header\nstring scan\n";
  static final boolean _IS_SERVICE = false;
  static final boolean _IS_ACTION = false;
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  java.lang.String getScan();
  void setScan(java.lang.String value);
}
