#include "arf_imu/imu.h"
#include "arf_imu/RazorAHRS.h"
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>

using namespace IRT;
using namespace IRT::Walker_pkg;

double n_acc = 9.81/256.0;
double n_deg_to_rad = 0.01745329252;
double n_gyr = 0.01745329252/14.375;
int cnt = 0; //ADDED
double yaw_calibration = 0; //ADDED

static const std::string IMU_FRAME_ID = "imu_frame"; 

ImuDevice* ImuDevice::ms_objref = NULL;

//ImuDevice::getMainThread - singleton
ImuDevice* ImuDevice::getMainThread(void)
{
    std::cout << "Calling singleton\n";
    static ImuDevice obj;
    //store in static ref for future ref
    ms_objref = &obj;
    return &obj;
}


//Private constructor
ImuDevice::ImuDevice() : m_running(false)
{
     std::cout << "CASImu constructor\n";
}

//Destructor
ImuDevice::~ImuDevice()
{
    std::cout << "CASImu destructor\n";
}

//Callback for IMU library
void ImuDevice::imu_callback(const float data[])
{

    //ADDED
    if(cnt == 0){
    yaw_calibration = data[9];
    cnt++;
    }

    /* //trying to get the average over 200 yaw readings and get the initial yaw value
    if(cnt <= 199){
    yaw_calibration = yaw_calibration + data[9];
    cnt++;
       if(cnt == 199){
          yaw_calibration = yaw_calibration / 200;
       }
    return;
    }
    */
    //ADDED

    /* RAW SENSOR OUTPUT  */
    // Raw accelerometer output
    ms_objref->m_imuraw.accelerometer.x = data[0];
    ms_objref->m_imuraw.accelerometer.y = data[1];
    ms_objref->m_imuraw.accelerometer.z = data[2];
    // Raw magnetometer output
    ms_objref->m_imuraw.magnetometer.x = data[3];
    ms_objref->m_imuraw.magnetometer.y = data[4];
    ms_objref->m_imuraw.magnetometer.z = data[5];
    // Raw gyroscope output
    ms_objref->m_imuraw.gyroscope.x = data[6];
    ms_objref->m_imuraw.gyroscope.y = data[7];
    ms_objref->m_imuraw.gyroscope.z = data[8];
    // Calibrated RPY output (in degrees)
    ms_objref->m_imuraw.RPY.x = data[11];
    ms_objref->m_imuraw.RPY.y = data[10];
    ms_objref->m_imuraw.RPY.z = data[9];
    // time stamp
    ms_objref->m_imuraw.header.stamp = ros::Time::now();
    // Set Frame ID
    ms_objref->m_imuraw.header.frame_id = IMU_FRAME_ID;

    /* SENSOR OUTPUT HUMAN READABLE */

    // Roll Pitch Yaw (in radians)
    ms_objref->m_imureal.RPY.x = data[11]*n_deg_to_rad;  //roll
  //ms_objref->m_imureal.RPY.y = data[10]*n_deg_to_rad; //pitch - Before -1
  //ms_objref->m_imureal.RPY.z = data[9]*n_deg_to_rad; //yaw - Before -1
    ms_objref->m_imureal.RPY.y = -data[10]*n_deg_to_rad; //pitch
  //ms_objref->m_imureal.RPY.z = -data[9]*n_deg_to_rad; //yaw, with only the minus implementation
    //ADDED
    double temp_yaw = -(data[9]-yaw_calibration);
    if (temp_yaw > 180.0){
            temp_yaw = temp_yaw - 360.0;
        }
    else if(temp_yaw < -180.0){
            temp_yaw = temp_yaw + 360.0;
        }
    ms_objref->m_imureal.RPY.z = temp_yaw*n_deg_to_rad;
    //ADDED

    // Quaternion from RPY
    ms_objref->m_imureal.orientation = tf::createQuaternionMsgFromRollPitchYaw(data[11]*n_deg_to_rad, data[10]*n_deg_to_rad, data[9]*n_deg_to_rad);

    // Linear acceleration (m/s^2)
  //ms_objref->m_imureal.linear_acceleration.x = data[0]*n_acc; // Before -1
    ms_objref->m_imureal.linear_acceleration.x = -data[0]*n_acc;
    ms_objref->m_imureal.linear_acceleration.y = data[1]*n_acc;
    ms_objref->m_imureal.linear_acceleration.z = data[2]*n_acc;

    // Raw gyroscope output (rad/s)
    ms_objref->m_imureal.angular_velocity.x = data[6]*n_gyr;
  //ms_objref->m_imureal.angular_velocity.y = data[7]*n_gyr;   // Before -1
  //ms_objref->m_imureal.angular_velocity.z = data[8]*n_gyr;   // Before -1
    ms_objref->m_imureal.angular_velocity.y = -data[7]*n_gyr;
    ms_objref->m_imureal.angular_velocity.z = -data[8]*n_gyr;

    // time stamp
    ms_objref->m_imureal.header.stamp = ros::Time::now();
    // Set Frame ID
    ms_objref->m_imureal.header.frame_id = IMU_FRAME_ID;

    ms_objref->m_imurawpub.publish(ms_objref->m_imuraw);
    ms_objref->m_imurealpub.publish(ms_objref->m_imureal);
}

//IMU error handling
void ImuDevice::imu_onerror(const string &msg)
{
    ROS_INFO("%s",msg.c_str());
}

//Start IMU ros node
void ImuDevice::start(ros::NodeHandle & n_)
{

    assert(m_running==false);

    //read the port
    n_.param<std::string>("port", m_usbport, "/dev/arfimu");

    std::cout << "USB: " << m_usbport << "\n";

    m_running = true;

    try
    {
       RazorAHRS *razor = new RazorAHRS(m_usbport, ImuDevice::imu_callback, ImuDevice::imu_onerror, RazorAHRS::ACC_MAG_GYR_CALIBRATED_YAW_PITCH_ROLL);
    }
    catch(runtime_error &e)
    {
        cout << "  " << (string("Could not create tracker: ") + string(e.what())) << endl;
        cout << "  " << "Did you set your serial port in Example.cpp?" << endl;
        return;
    }

}


//Handle subscription to ROS topic
void ImuDevice::advertiseRos(ros::NodeHandle & n_)
{
    m_imurawpub = n_.advertise<arf_msgs::imuRaw> ("imuraw", 1);
    m_imurealpub = n_.advertise<arf_msgs::imuReal> ("imureal", 1);

    ros::spin();
}

//Stop IMU rosnode
void ImuDevice::reset(void)
{
    m_running = false;
    std::cout << "ImuDevice thread joined\n";
}

int main(int argc, char **argv)
{
    ImuDevice * dObj = ImuDevice::getMainThread();

    ros::init(argc, argv, "casimu");

    ros::NodeHandle n=ros::NodeHandle("~");

    //Start ImuDevice thread
    dObj->start(n);

    dObj->advertiseRos(n);

    std::cout << "Exit Ros spin\n";

    dObj->reset();

    sleep(1);
    std::cout << "Terminate CASImu application\n";

    n.shutdown();
    return 0;
}

