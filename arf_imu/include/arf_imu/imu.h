/*
 * imu.h
 *
 * Copyright (c) 2013, Centre for Intelligent Mechatronics Systems, University of Technology, Sydney, Australia.
 * All rights reserved.
 *
 * This software was developed as a part of an industry based research project on Assistive Robotic Devices.
 *
 * Author: Ravindra Ranasinghe
 * Date: 04/04/2013
 *
 * Rev History:
 *       0.0 - Ravindra Ranasinghe
 *       0.1 - Christopher Andronikos
 *       0.2 - Ravindra Ranasinghe
 */

#ifndef __CAS_RAZORIMU_HEADER__
#define __CAS_RAZORIMU_HEADER__

#include <ros/ros.h>
#include <arf_msgs/imuRaw.h>
#include <arf_msgs/imuReal.h>
#include <tf/tf.h>
#include <signal.h>
#include <string.h> 
#include <pthread.h>
#include <termios.h> 
#include <vector> 

//IRT project namespace
namespace IRT
{

//Namespace for walker
namespace Walker_pkg
{

//Class
class ImuDevice 
{

public:
    //Singleton
    static ImuDevice * getMainThread(void);
  
    //Destructor
    virtual ~ImuDevice();
  
   
     //Modifiers
     void  start(ros::NodeHandle & n_);

     void reset(void);
     
     void advertiseRos(ros::NodeHandle & n_);
private:
     //Private Methods
  
     //ImuDevice static callback from the library
     static void  imu_callback(const float data[]);
  
     //ImuDevice static error callback from the library
     static void  imu_onerror(const std::string &msg);
     //Private constructor
     ImuDevice();

private:
     //Private Members
  
     bool               m_running;
     std::string        m_usbport;
     //publishers
     ros::Publisher     m_imurawpub;
     ros::Publisher     m_imurealpub;
     //messages
     arf_msgs::imuRaw m_imuraw;
     arf_msgs::imuReal m_imureal;
     //pointer to self
     static ImuDevice*     ms_objref;
};

}

}

#endif
