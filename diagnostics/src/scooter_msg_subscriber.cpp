/**
    Implementation of ScooterMsgSubscriber class
*/

#include <scooter/scooter_msg_subscriber.hpp>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <arf_msgs/encoder.h>
#include <arf_msgs/imuReal.h>
//#include <darknet_ros_msgs/BoundingBoxes.h>
#include <sensor_msgs/NavSatFix.h>

ScooterMsgSubscriber::ScooterMsgSubscriber(std::string cam1Topic, std::string cam2Topic, std::string camTopic, std::string encoderTicksTopic, std::string imuTopic, 
    std::string yoloTopic, std::string gpsTopic, ros::NodeHandle nh, int numTopics) {
    this->cam1Topic = cam1Topic;
    this->cam2Topic = cam2Topic;
    this->camTopic = camTopic;
    this->encoderTicksTopic = encoderTicksTopic;
    this->imuTopic = imuTopic;
    this->yoloTopic = yoloTopic;
    this->gpsTopic = gpsTopic;

    this->nh = nh;

    this->numTopics = numTopics;
    this->countMsgs = 0;
}

void ScooterMsgSubscriber::cam1Cb(const sensor_msgs::Image::ConstPtr msg) {
    this->countMsgs++; // roscpp subscriber thread safety assumed
    ROS_INFO("Message received on topic : %s ", this->cam1Topic.c_str());
    this->cam1Subscriber.shutdown();
}

void ScooterMsgSubscriber::cam2Cb(const sensor_msgs::Image::ConstPtr msg) {
    this->countMsgs++; // roscpp subscriber thread safety assumed
    ROS_INFO("Message received on topic : %s ", this->cam2Topic.c_str());
    this->cam2Subscriber.shutdown();
}

void ScooterMsgSubscriber::camCb(const sensor_msgs::Image::ConstPtr msg) {
    this->countMsgs++; // roscpp subscriber thread safety assumed
    ROS_INFO("Message received on topic : %s ", this->camTopic.c_str());
    this->camSubscriber.shutdown();
}

void ScooterMsgSubscriber::encoderCb(const arf_msgs::encoder::ConstPtr msg) {
    this->countMsgs++; // roscpp subscriber thread safety assumed
    ROS_INFO("Message received on topic : %s ", this->encoderTicksTopic.c_str());
    this->encoderSubscriber.shutdown();
}

void ScooterMsgSubscriber::imuCb(const arf_msgs::imuReal::ConstPtr msg) {
    this->countMsgs++; // roscpp subscriber thread safety assumed
    ROS_INFO("Message received on topic : %s ", this->imuTopic.c_str());
    this->imuSubscriber.shutdown();
}

//void ScooterMsgSubscriber::yoloCb(const darknet_ros_msgs::BoundingBoxes::ConstPtr msg) {
//    this->countMsgs++; // roscpp subscriber thread safety assumed
//    ROS_INFO("Message received on topic : %s ", this->yoloTopic.c_str());
//    this->yoloSubscriber.shutdown();
//}

void ScooterMsgSubscriber::gpsCb(const sensor_msgs::NavSatFix::ConstPtr msg) {
    this->countMsgs++; // roscpp subscriber thread safety assumed
    ROS_INFO("Message received on topic : %s ", this->gpsTopic.c_str());
    this->gpsSubscriber.shutdown();
}

void ScooterMsgSubscriber::addSubscribers() {
    this->cam1Subscriber = this->nh.subscribe(
        this->cam1Topic,
        10,
        &ScooterMsgSubscriber::cam1Cb,
        this
    );

    this->cam2Subscriber = this->nh.subscribe(
        this->cam2Topic,
        10,
        &ScooterMsgSubscriber::cam2Cb,
        this
    );

    this->camSubscriber = this->nh.subscribe(
        this->camTopic,
        10,
        &ScooterMsgSubscriber::camCb,
        this
    );

    this->encoderSubscriber = this->nh.subscribe(
        this->encoderTicksTopic,
        10,
        &ScooterMsgSubscriber::encoderCb,
        this
    );

    this->imuSubscriber = this->nh.subscribe(
        this->imuTopic,
        10,
        &ScooterMsgSubscriber::imuCb,
        this
    );

  //  this->yoloSubscriber = this->nh.subscribe(
  //      this->yoloTopic,
  //      10,
  //      &ScooterMsgSubscriber::yoloCb,
  //      this
  //  );

    this->gpsSubscriber = this->nh.subscribe(
        this->gpsTopic,
        10,
        &ScooterMsgSubscriber::gpsCb,
        this
    );
}

void ScooterMsgSubscriber::diagnoseMsgs() {

    ROS_INFO("Starting diagnostics...");

    while(this->numTopics != this->countMsgs && ros::ok()) {
        ros::spinOnce();
    }

    ROS_INFO("\n \
    ******************************************************************\n \
    ******************************************************************\n \
                                OK  \n \
    ******************************************************************\n \
    ******************************************************************\n \
    ");
}
