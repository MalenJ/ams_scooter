/**
    Subscribe to a list of topics and make sure each topic has messages published
*/
#include <ros/ros.h>
#include "scooter/scooter_msg_subscriber.hpp"

static const std::string CAM1_TOPIC = "/cam_1/color/image_raw";
static const std::string CAM2_TOPIC = "/cam_2/color/image_raw";
static const std::string CAM_TOPIC = "/cam";
static const std::string ENCODER_TOPIC = "/encoder_ticks";
static const std::string IMU_TOPIC = "/casimu/imureal";
//static const std::string YOLO_TOPIC = "darknet_ros/bounding_boxes";
static const std::string GPS_TOPIC = "/piksi/navsatfix_best_fix";


int main(int argc, char** argv) {
    ros::init(argc, argv, "scooter_msg_diagnostic");

    ros::NodeHandle n;

    std::string cam1Topic, cam2Topic, camTopic, encoderTopic, imuTopic, yoloTopic, gpsTopic;

    // Use parameters in the launch file to change the default topic names
    n.param<std::string>("cam1_topic", cam1Topic, CAM1_TOPIC);
    n.param<std::string>("cam2_topic", cam2Topic, CAM2_TOPIC);
    n.param<std::string>("cam_topic", camTopic, CAM_TOPIC);
    n.param<std::string>("endocer_topic", encoderTopic, ENCODER_TOPIC);
    n.param<std::string>("imu_topic", imuTopic, IMU_TOPIC);
    //n.param<std::string>("yolo_topic", yoloTopic, YOLO_TOPIC);
    n.param<std::string>("gps_topic", gpsTopic, GPS_TOPIC);

    ScooterMsgSubscriber scooterMsgSubscriber(
        cam1Topic, 
        cam2Topic, 
        camTopic, 
        encoderTopic, 
        imuTopic, 
      //  yoloTopic, 
        gpsTopic, 
        n, 
        6
    );

    scooterMsgSubscriber.addSubscribers();

    scooterMsgSubscriber.diagnoseMsgs();

    return 0;
}


