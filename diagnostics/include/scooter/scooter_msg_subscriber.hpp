/*
    Subscriber class for scooter messages
*/

#ifndef SCOOTER_MSG_SUBSCRIBER_H
#define SCOOTER_MSG_SUBSCRIBER_H

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <arf_msgs/imuReal.h>
#include <arf_msgs/encoder.h>
//#include <darknet_ros_msgs/BoundingBoxes.h>
#include <sensor_msgs/NavSatFix.h>

class ScooterMsgSubscriber {
    public :
        // cam1, cam2, cam, encoder, imu, yolo, gps
        ScooterMsgSubscriber(std::string, std::string, std::string, std::string, std::string, std::string, std::string, ros::NodeHandle, int numTopics);

        void addSubscribers();

        void diagnoseMsgs();
    
    private :
        std::string cam1Topic;
        std::string cam2Topic;
        std::string camTopic;
        std::string encoderTicksTopic;
        std::string imuTopic;
        std::string yoloTopic;
        std::string gpsTopic;

        ros::NodeHandle nh;

        int numTopics;
        int countMsgs;

        ros::Subscriber cam1Subscriber;
        ros::Subscriber cam2Subscriber;
        ros::Subscriber camSubscriber;
        ros::Subscriber encoderSubscriber;
        ros::Subscriber imuSubscriber;
    //    ros::Subscriber yoloSubscriber;
        ros::Subscriber gpsSubscriber;

        void cam1Cb(const sensor_msgs::Image::ConstPtr);
        void cam2Cb(const sensor_msgs::Image::ConstPtr);
        void camCb(const sensor_msgs::Image::ConstPtr);
        void encoderCb(const arf_msgs::encoder::ConstPtr);
        void imuCb(const arf_msgs::imuReal::ConstPtr);
      //  void yoloCb(const darknet_ros_msgs::BoundingBoxes::ConstPtr);
        void gpsCb(const sensor_msgs::NavSatFix::ConstPtr);
};


#endif


