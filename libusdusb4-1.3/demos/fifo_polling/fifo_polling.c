#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include "libusdusb4.h"


#define FALSE 0
#define TRUE  1


#define RECORDS_TO_READ 10
static USB4_FIFOBufferRecord BufferRecord[RECORDS_TO_READ];



// Pause between reads of the USB4 counter
// in milliseconds.
//
const int COUNT_READ_PAUSE = 50000;




// Rather than getting into complications reading the
// keyboard for input, we will just trap Ctrl+C and 
// notify the app when the user is done.
//
int CloseRequested = FALSE;

void ctrlCHandler(int signal)
{
    CloseRequested = TRUE;
}




int main(int argc, char* argv[])
{
    printf("--------------------------------\n");
    printf("USB4 POLLING FIFO BUFFER!\n");
    printf("--------------------------------\n");


    // Register the Ctrl-C handler.
    signal(SIGINT, ctrlCHandler);


    //
    // Caution! Most of this example is implemented without any error checking.
    //


    // Initialize the USB4 driver.
    short deviceCount = 0;
    int result = USB4_Initialize(&deviceCount);

	// Check result code...
	if (result != USB4_SUCCESS) 
    {
		printf("Failed to initialize USB4 driver!  Result code = %d.\n", result);
		USB4_Shutdown();
        return -1;
	}


    // Caution! The rest of the example is implemented without any error checking.


    short deviceNumber = 0;

    // Configure encoder channel 0.
    int channel = 0;
    USB4_SetPresetValue(deviceNumber, channel, 499);		// Set the preset register to the CPR-1
    USB4_SetMultiplier(deviceNumber, channel, 3);			// Set quadrature mode to X4.
	USB4_SetCounterMode(deviceNumber, channel, 3);		// Set counter mode to modulo-N.
    USB4_SetForward(deviceNumber, channel, 1);			// Optional: determines the direction of counting.
	USB4_SetTriggerOnIncrease(deviceNumber, channel, 1);	// Trigger capture when counter 0 increases.
	USB4_SetTriggerOnDecrease(deviceNumber, channel, 1);	// Trigger capture when counter 0 decreases.
	USB4_SetCaptureEnabled(deviceNumber, channel, 1);		// Enable capture on channel 0.
    USB4_SetCounterEnabled(deviceNumber, channel, 1);		// Enable the counter. **IMPORTANT**
    

    // USB4_SetControlMode(0, 0, 0x84f000);			// You may replace the previous five lines with 
                                            		// one call to USB4_SetControlMode using to correct
                                            		// control mode value.

    // Clear the FIFO buffer
    USB4_ClearFIFOBuffer(deviceNumber);

    // Enable the FIFO buffer
    USB4_EnableFIFOBuffer(deviceNumber);

    // Clear the captured status register
    USB4_ClearCapturedStatus(deviceNumber, 0);

    printf("Reading encoder channel %d. Press any key to exit.\n",channel);


    int recordsRead = 0;
    int fifoCount = 0;

	// Keep reading the FIFO buffer until we get the number of records requested.
	while((recordsRead < RECORDS_TO_READ) && (CloseRequested == FALSE)) 
    {
		fifoCount = (RECORDS_TO_READ - recordsRead);

		// fifoCount will be set to the number of records copied.
		result = USB4_ReadFIFOBufferStruct(deviceNumber, &fifoCount, &BufferRecord[recordsRead], 1000);  
		recordsRead += fifoCount;
    }

	if (recordsRead == RECORDS_TO_READ) 
    {

		for (int i = 0; i < recordsRead; i++) 
        {
			printf("%3i: Timestamp: %10u  CH0: %10u  CH1: %10u  CH2: %10u  CH3: %10u  Input: %10u\n", 
				i,				
				BufferRecord[i].Time, 
				BufferRecord[i].Count[0], 
				BufferRecord[i].Count[1], 
				BufferRecord[i].Count[2], 
				BufferRecord[i].Count[3], 
				BufferRecord[i].Input);
		}
	}
	printf("Records Read = %d\n", recordsRead);


    // Close all open connections to the USB4 devices.
    USB4_Shutdown();

	printf("Done.");
    return 0;
}


