#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include "/usr/include/libusdusb4.h"


#define FALSE 0
#define TRUE  1



// Pause between reads of the USB4 counter
// in milliseconds.
//
const int COUNT_READ_PAUSE = 50000;




// Rather than getting into complications reading the
// keyboard for input, we will just trap Ctrl+C and 
// notify the app when the user is done.
//
int CloseRequested = FALSE;

void ctrlCHandler(int signal)
{
    CloseRequested = TRUE;
}




int main(int argc, char* argv[])
{
	printf("--------------------------------\n");
	printf("USB4 Hello World!\n");
	printf("--------------------------------\n");


    // Register the Ctrl-C handler.
    signal(SIGINT, ctrlCHandler);

	
    // Initialize the USB4 driver.
    short deviceCount = 0;
    int result = USB4_Initialize(&deviceCount);
	
    // Check result code...
    if (result != USB4_SUCCESS)
    {
        printf("Failed to initialize USB4 driver!  Result code = %d.\nPress Ctrl-C to exit.\n", result);
    }
    else
    {
	// Caution! The reset of the example is implemented without any error checking.


	// Configure encoder channel 0.
	USB4_SetPresetValue(0,0,499);		// Set the preset register to the CPR-1
	USB4_SetMultiplier(0,0,3);			// Set quadrature mode to X4.
	USB4_SetCounterMode(0,0,3); 		// Set counter mode to modulo-N.
	USB4_SetForward(0,0,TRUE);			// Optional: determines the direction of counting.
	USB4_SetCounterEnabled(0,0,TRUE);	// Enable the counter. **IMPORTANT**
	USB4_ResetCount(0,0);				// Reset the counter to 0

	// USB4_SetControlMode(0,0,0xFC000);	// You may replace the previous five lines with 
	// one call to USB4_SetControlMode using to correct
	// control mode value.
		
        printf("Reading encoder channel 0. Press Ctrl-C to exit.\n");

        unsigned int currentCount;
        unsigned int previousCount = 0xFFFFFFFF;

        // Waits for the user to press any key, then exits.
        while (CloseRequested == FALSE)
        {
            USB4_GetCount(0,0,&currentCount);

            // Update display when value changes
            if (previousCount != currentCount)
            {   
                printf("%d    \r", currentCount);
                fflush(stdout);
            }
            previousCount = currentCount;
            usleep(COUNT_READ_PAUSE);
        }
    }

    // Close all open connections to the USB4 devices.
    USB4_Shutdown();

    printf("\n");
	
    return 0;
}
