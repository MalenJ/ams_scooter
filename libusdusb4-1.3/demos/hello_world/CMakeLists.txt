cmake_minimum_required(VERSION 2.6)

# Project set up.
project(USB4_HelloWorld)

set(CMAKE_BUILD_TYPE Debug)
#set(CMAKE_BUILD_TYPE Release)

include_directories(/usr/include)


# C/C++ compiler flags
set(C_FLAGS "-std=gnu99 -pedantic -pthread -Wall")
set(CXX_FLAGS  "-ansi -pedantic -pthread -Wall -Weffc++")

set(CMAKE_C_FLAGS_DEBUG "-g ${C_FLAGS}")
set(CMAKE_C_FLAGS_RELEASE ${C_FLAGS})

set(CMAKE_CXX_FLAGS_DEBUG "-g ${CXX_FLAGS}")
set(CMAKE_CXX_FLAGS_RELEASE ${CXX_FLAGS})



#########################
# Targets
#########################


add_library(libUsb4 SHARED IMPORTED)
set_property(TARGET libUsb4 PROPERTY IMPORTED_LOCATION /usr/lib/libusdusb4.so)

add_executable(hello_world hello_world.c)
target_link_libraries(hello_world libUsb4)


