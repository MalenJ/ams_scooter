/* ConsoleTimeBasedDataLogging.cpp : Defines the entry point for the console application.

    This example illustrates how to use the data logging feature of the USB4 card.
    General steps are as follows.

    Step  1: Initialize USB4.
    Step  2: Set the bit 23 (capture) of control registers to '1'. (Other bits of control registers must also be set to your desired settings.)
    Step  3: Select the sampling period.
    Step  4: Select the number of samples to be collected and the condition for triggering and storage qualification.
    Step  5: Clear the FIFO buffer.
    Step  6: Turn on the FIFO buffer.
    Step  7: Start acquisition.
    Step  8: Read data from the FIFO until the specified number of data are collected.
    Step  9: Display the collected data
    Step 10: Shutdown
*/


#include <stdio.h>
#include "libusdusb4.h"



#define SAMPLES_TO_COLLECT 50




int main(int argc, char* argv[])
{
    short deviceNumber = 0; // Used to select the device we want to use.
    short deviceCount = 0; // Used to get the number of USB4 devices found.


    int result = USB4_SUCCESS;

    USB4_FIFOBufferRecord samples[SAMPLES_TO_COLLECT];


    int samplesCollected = 0;
    int prevousSampleCount = 0;

    int bufferCount = 0;
    unsigned char encoderChannels = 0;
    unsigned int timeDifference = 0;

    // 0 = Ignore, 1 = Rising, 2 = Falling, 3 = Change, 4 = High, 5 = Low. 6 = Always, 7 = Always
    unsigned char trigger1Array[8] = {3,0,0,0,0,0,0,0}; // Condition1: Start acquisition on change of input bit 0.  
    unsigned char trigger2Array[8] = {6,6,6,6,6,6,6,6}; // Condition2: Set to always for all input bits.
                                                        
    unsigned char trigger1And = 0;                      // 0 = OR trigger condition for all inputs, 
                                                        // 1 = AND trigger conditions for all inputs
    unsigned char trigger2And = 0;                      // 0 = OR qualifier conditions for inputs
                                                        // 1 = AND

    unsigned char  adcTriggerArray[4] = {0};
    unsigned short adcThreshold[4] = {0};
    unsigned char  pwmTriggerArray[4] = {0};
    unsigned int  pwmThresholdArray[4] = {0};

    printf("US Digital USB4 Data Logging Demo\n\n");
    printf("Press 1 to start data logging immediately.\n");
    printf("Press 2 to start data logging when logic level of input 0 of I/O port changes.\n");
    printf("Press 3 to quit demo.\n");
    
    char choice;
    do 
    {
        choice = getchar();
    } while (choice < '1' || choice > '3');

    if ((choice == '1') || (choice == '2'))
    {
        // Step 1: Initialize the USB4 driver.
        result = USB4_Initialize(&deviceCount);

        // Check result code...
        if (result != USB4_SUCCESS)
        {
            printf("Failed to initialize USB4 driver!  Result code = %d.\n", result);
        }
        else
        {
            // Check how many boards were detected...
            if (deviceCount < 1)
            {
                result = FATAL_ERROR;
                printf("No USB4 boards detected!\n");
            }
            else
            {
                printf("deviceCount=  %d\n",deviceCount);

                // Step 2: Enable capture, quadrature mode to X1, counter mode to 24 bit counter, 
                //         and enable the counters.
                int failedEncoder = 0;
                for (int i = 0; i < USB4_MAX_ENCODERS; i++)
                {
                    result = USB4_SetControlMode(deviceNumber, i, 0x844000);
                    if(result != 0)
                    {
                        printf("Failed to set the control mode for encoder %d!\n", i);
                        failedEncoder = i;
                        break;
                    }
                }
                if (result == USB4_SUCCESS)
                {
                    // Step 3: Set the sampling period. reg.#30 (n+1 * 2�s) 
                    // X = number of seconds desired. N = value to place in register 30.
                    // N = ((X * 10^6) / 2) -1
                    // 49999 = (.1 * 1000000) / 2 - 1 
                    // The value of N for an aproximate 100 ms sampling period would be 49999.
                    result = USB4_SetSamplingRateMultiplier(deviceNumber, 49999);
                    if (result != 0)
                    {
                        printf("Failed to set the sampling rate multiplier%d!\n", failedEncoder);
                    }
                }
            }
        }
        if (result == USB4_SUCCESS)
        {
            printf("Samples To Collect: %d\n", SAMPLES_TO_COLLECT);
            if (choice == '1')
            {
                // User chose to start acquisition immediately.  So, set input triggering condition to Always.
                trigger1Array[0] = 6;   
                trigger1Array[1] = 6;   
                trigger1Array[2] = 6;
                trigger1Array[3] = 6;   
                trigger1Array[4] = 6;   
                trigger1Array[5] = 6;   
                trigger1Array[6] = 6;   
                trigger1Array[7] = 6;   
            }
            else
            {
                // User chose to start acquisition on change of input bit 0.    
                printf("Waiting for a change in logic level of input 0 of I/O port...\n");
            }
            
            // Note: No error checking occurs from this point on

            // Step 4: Select the condition for triggering and storage qualification 
            // and number of samples to be collected. reg.#41, reg.#42, reg.#43
            // Trigger1 condition set to Always for all input, 
            // Trigger2 condition set to Always for all input.
            // Trigger1 condition for all bits are OR'd together and Trigger2 conditions 
            // for all bits are OR'd together.
            // See intialization of variables above.
            result = USB4_SetTimeBasedLogSettings(deviceNumber, 
                                                   trigger1Array, trigger1And, 
                                                   trigger2Array, trigger2And, 
                                                   adcTriggerArray, adcThreshold,
                                                   pwmTriggerArray, pwmThresholdArray,
                                                   encoderChannels, SAMPLES_TO_COLLECT);

            // Step 5: Clear the FIFO buffer. reg.#38
            result = USB4_ClearFIFOBuffer(deviceNumber);

            // Step 6: Enable FIFO. reg.#37
            result = USB4_EnableFIFOBuffer(deviceNumber);

            // Step 7: Start acquisition. reg.#45
            result = USB4_StartAcquisition(deviceNumber);

            // Step 8: Read data from the FIFO until the specified number of records are collected.
            unsigned long timeout = 0;
            while (samplesCollected < SAMPLES_TO_COLLECT && (result == 0 || result == FIFO_BUFFER_EMPTY))
            {
                bufferCount = SAMPLES_TO_COLLECT - samplesCollected;
                result = USB4_ReadFIFOBufferStruct(deviceNumber, &bufferCount, 
                                                   &samples[samplesCollected], timeout);
                samplesCollected += bufferCount;
                
                if (prevousSampleCount != samplesCollected)
                {
                    printf("\rSamples Collected: %d", samplesCollected);
                    fflush(stdout);
                }

                prevousSampleCount = samplesCollected;
            }

            printf("\n\n%10s%10s%10s%10s%10s%25s\n", 
                    "TimeStamp", "Count0", "Count1", "Count2", "Input", "TimeStamp Differrence");

            printf("(unit: 21ns)                                       (unit: 21ns) (unit: sec)\n");
            printf("---------------------------------------------------------------------------\n");
             
            // Step 9: Display the collected sample data.
            for (int i = 0; i < samplesCollected; i++) 
            {
                if (i > 0)
                {
                    timeDifference = samples[i].Time - samples[i-1].Time;
                }

                printf("%10u%10u%10u%10u%10u%10d\t%f\n", 
                       samples[i].Time, 
                       samples[i].Count[0], samples[i].Count[1], samples[i].Count[2], 
                       samples[i].Input, timeDifference, timeDifference * 0.000000021);
            }
        }

        // Step 10: Shutdown
        USB4_Shutdown();
    }
    return 0;
}
    
