#include <stdio.h>
#include <sys/time.h>
#include <signal.h>
#include "libusdusb4.h"




#define SAMPLES_TO_COLLECT 12000



// Rather than getting into complications reading the
// keyboard for input, we will just trap Ctrl+C and 
// notify the app when the user is done.
//
int CloseRequested = FALSE;

void ctrlCHandler(int signal)
{
    CloseRequested = TRUE;
}


int getCurrentTime()
{
    struct timeval timeOfDay;
    gettimeofday(&timeOfDay, NULL);

    return ((timeOfDay.tv_sec * 1000) + (timeOfDay.tv_usec / 1000.0)) + 0.5;
}


void runPollingTest(int deviceNumber)
{
    unsigned int currentTimeStamp;
    unsigned int firstTimeStamp = 0;
    unsigned int lastTimeStamp = 0;
    unsigned int counts[4] = {0,0,0,0};

    printf("Polling CaptureTimeAndCounts...: %d\n", SAMPLES_TO_COLLECT);

    for(int i = 0; i < 1000; i++)
    {
        USB4_CaptureTimeAndCounts(deviceNumber, counts, &currentTimeStamp);
        if (i == 0)
        {
            firstTimeStamp = currentTimeStamp;
        }
        else
        {
            lastTimeStamp = currentTimeStamp;
        }
    }
    
    printf("The average time to capture all four encoder channels and timestamp is %f seconds.\n" , 
           ((lastTimeStamp - firstTimeStamp) / 1000) * 0.000000021); 
}



void runFIFOThroughputTest(int deviceNumber)
{
    printf("Running FIFO Throughput Test...:\n");

    // 0 = Ignore, 1 = Rising, 2 = Falling, 3 = Change, 4 = High, 5 = Low. 6 = Always, 7 = Always
    unsigned char trigger1Array[8] = {6,6,6,6,6,6,6,6}; // Start acquisition immediately.
    unsigned char trigger2Array[8] = {6,6,6,6,6,6,6,6}; // All samples qualify for storage.
                                                        
    unsigned char trigger1And = 0;                      // 0 = OR trigger condition for all inputs, 
                                                        // 1 = AND trigger conditions for all inputs
    unsigned char trigger2And = 0;                      // 0 = OR qualifier conditions for inputs
                                                        // 1 = AND

    unsigned char  adcTriggerArray[4] = {0,0,0,0};
    unsigned short adcThresholdArray[4] = {0,0,0,0};
    unsigned char  encoderChannels = 0;
    unsigned int   numberOfSamples = 0;

    // Select the condition for triggering and storage qualification 
    // and number of samples to be collected. reg.#41, reg.#42, reg.#43
    // Trigger condition set to Always for all input, Qualifier condition set to Always for all input, 
    // trigger condition for all bits are OR'd together, qualifier conditions for all bits are OR'd together.
    // See intialization of variables above.
    unsigned char pwmTriggerArray[4] = {0,0,0,0};
    unsigned int  pwmThresholdArray[4] = {0,0,0,0};
    
    USB4_SetTimeBasedLogSettings(deviceNumber, 
                                 trigger1Array, trigger1And, 
                                 trigger2Array, trigger2And, 
                                 adcTriggerArray, adcThresholdArray,
                                 pwmTriggerArray, pwmThresholdArray,
                                 encoderChannels,
                                 numberOfSamples);

    // Clear the FIFO buffer. reg.#38
    USB4_ClearFIFOBuffer(deviceNumber);

    // Enable FIFO. reg.#37
    USB4_EnableFIFOBuffer(deviceNumber);

    // Start acquisition. reg.#45
    USB4_StartAcquisition(deviceNumber);

    // Start off slow and then pick up speed if we can keep up.

    // Set the sampling period. reg.#30 (n+1 * 2�s) 
    // X = number of seconds desired. N = value to place in register 30.
    // N = ((X * 10^6) / 2) -1
    // 499 = (.001 * 1000000) / 2 - 1 The value of N for an aproximate 10 ms sampling period would be 499.
    // Start off at 10ms.
    unsigned int sampleRate = 30;
    int result = USB4_SetSamplingRateMultiplier(deviceNumber, sampleRate);
    if(result != 0) 
    {
        printf("Failed to set the sampling rate multiplier.");
        return;
    }
    

    printf("Press Ctrl-C to exit.\n");

    unsigned int timeout = 0;
    unsigned int bufferCount = 0;
    unsigned int expired = 0;

    int samplesRead = 0;
    USB4_FIFOBufferRecord samples[SAMPLES_TO_COLLECT];


    // Read data from the FIFO until the specified number of records are collected.
    while (sampleRate && (result == 0 || result == FIFO_BUFFER_EMPTY) && (CloseRequested == FALSE)) 
    {
        samplesRead = SAMPLES_TO_COLLECT;

        USB4_ReadFIFOBufferStruct(deviceNumber, &samplesRead, samples, timeout);
        result = USB4_GetFIFOBufferCount(deviceNumber, &bufferCount);

        printf("\r Buffer Level = %u, Frequency set to %u microseconds between samples...", 
               bufferCount, (sampleRate-1) * 2);
        fflush(stdout);

        if (bufferCount < 1000 && (expired < getCurrentTime()))
        {
            // We must decrease speed.
            sampleRate -= 2;
            result = USB4_SetSamplingRateMultiplier(deviceNumber, sampleRate);
            expired = getCurrentTime() + 3000;

            if (sampleRate < 4)
                break;
        }
        else if(bufferCount > 10000 && (expired < getCurrentTime()))
        {
            // We can increase speed.
            sampleRate += 2;
            result = USB4_SetSamplingRateMultiplier(deviceNumber, sampleRate);
            
            expired = getCurrentTime() + 3000;
            if (sampleRate > 100)
                break;
        }
    }
}
    


int main(int argc, char* argv[])
{
    // Register the Ctrl-C handler.
    signal(SIGINT, ctrlCHandler);


    // Select the device we want to use.
    short deviceNumber = 0;     


    short boardCount = 0;

    // Step 1: Initialize the USB4 driver.
    int result = USB4_Initialize(&boardCount);

    // Check result code...
    if (result != USB4_SUCCESS) 
    {
        printf("Failed to initialize USB4 driver.  Result code = %d.\n", result);
        goto done;
    }
    
    // Check how many boards were detected...
    if(boardCount < 1) 
    {
        printf("No USB4 boards detected.\n");
        goto done;
    }

    // Step 2: Enable capture, quadrature mode to X1, counter mode to 24 bit counter, and enable the counters.
    for (int i = 0; i < USB4_MAX_ENCODERS; i++) 
    {
        result = USB4_SetControlMode(deviceNumber, i, 0x844000);
        if(result != 0) 
        {
            printf("Failed to set the control mode for encoder %d!\n", i);
            goto done;
        }
    }

    printf("US Digital USB4 Data Logging Demo\n\n");
    printf("Press 1 to start polling test.\n");
    printf("Press 2 to start FIFO throughput test.\n");
    printf("Press 3 to quit demo.\n");

    char choice;
    do 
    {
        choice = getchar();
    } while (choice < '1' || choice > '3');

    switch (choice) 
    {
        case '1':
        {
            runPollingTest(deviceNumber);

            break;
        } 
        case '2':
        {
            runFIFOThroughputTest(deviceNumber);
            break;
        }
        case '3':
        {
            return 0;
            break;
        }
    }

done:
    USB4_Shutdown();
    printf("\n");

    return 0;
}

